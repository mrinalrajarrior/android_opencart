-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2019 at 08:20 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `anuska_ent`
--

-- --------------------------------------------------------

--
-- Table structure for table `contractors`
--

CREATE TABLE `contractors` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `start_date` date NOT NULL,
  `designation` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `dob` date NOT NULL,
  `father_name` varchar(50) NOT NULL,
  `suburb` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `postcode` varchar(50) NOT NULL,
  `home_phone` varchar(50) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(80) NOT NULL,
  `tax` varchar(255) NOT NULL,
  `cname` varchar(50) NOT NULL,
  `caddress` varchar(80) NOT NULL,
  `cphone` varchar(20) NOT NULL,
  `cmail` varchar(80) NOT NULL,
  `abn` varchar(50) NOT NULL,
  `acn` varchar(50) NOT NULL,
  `adhaar` varchar(50) NOT NULL,
  `pan` varchar(50) NOT NULL,
  `gst` varchar(25) NOT NULL,
  `acname` varchar(50) NOT NULL,
  `acnumber` varchar(25) NOT NULL,
  `ifsc` varchar(25) NOT NULL,
  `bank` varchar(30) NOT NULL,
  `branch` varchar(60) NOT NULL,
  `indian` int(1) NOT NULL,
  `permanent` int(1) NOT NULL,
  `visa` int(1) NOT NULL,
  `visa_exp` date NOT NULL,
  `restriction` varchar(80) NOT NULL,
  `kin` varchar(80) NOT NULL,
  `relationship` varchar(50) NOT NULL,
  `address2` varchar(80) NOT NULL,
  `suburb2` varchar(80) NOT NULL,
  `state2` varchar(50) NOT NULL,
  `postcode2` varchar(50) NOT NULL,
  `home_phone2` varchar(50) NOT NULL,
  `mobile2` varchar(50) NOT NULL,
  `work2` varchar(50) NOT NULL,
  `status` varchar(30) NOT NULL,
  `rate` varchar(30) NOT NULL,
  `rateper` varchar(30) NOT NULL,
  `dor` date NOT NULL,
  `source` varchar(50) NOT NULL,
  `pan2` varchar(50) DEFAULT NULL,
  `adhaar2` varchar(50) DEFAULT NULL,
  `gst2` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contractors`
--

INSERT INTO `contractors` (`id`, `first_name`, `last_name`, `start_date`, `designation`, `gender`, `dob`, `father_name`, `suburb`, `address`, `state`, `postcode`, `home_phone`, `mobile`, `email`, `tax`, `cname`, `caddress`, `cphone`, `cmail`, `abn`, `acn`, `adhaar`, `pan`, `gst`, `acname`, `acnumber`, `ifsc`, `bank`, `branch`, `indian`, `permanent`, `visa`, `visa_exp`, `restriction`, `kin`, `relationship`, `address2`, `suburb2`, `state2`, `postcode2`, `home_phone2`, `mobile2`, `work2`, `status`, `rate`, `rateper`, `dor`, `source`, `pan2`, `adhaar2`, `gst2`) VALUES
(1, 'Sumanta', 'Banerjee', '0000-00-00', 'Employee', 'Male', '2018-06-26', '', 'Kolkata', 'Kolkata, Kolkata', 'West Bengal', '700101', '7278075691', '', 'sumantab.9@live.in', '', 'Anuska Enterprise', 'Nandakumar', '1234567890', 'anuskaenterprise@gmail.com', '', '', '1435 0978 4999', 'MINPB5329M', 'GST2065065065011', '', '', '', '', '', 0, 0, 0, '0000-00-00', '', '', '', '', '', '', '', '', '', '', 'Select', '', '', '0000-00-00', 'construction', NULL, NULL, NULL),
(2, 'Sumanta', 'Banerjee', '2018-06-25', 'Employee', 'Male', '0000-00-00', '', 'Kolkata', 'Kolkata, Kolkata', 'West Bengal', '700101', '7278075691', '7278075691', 'sumantab.9@live.in', '', 'NextElectronics', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, 0, 0, '0000-00-00', '', '', '', '', '', '', '', '', '', '', 'Full Time', '', '', '0000-00-00', 'construction', NULL, NULL, NULL),
(3, 'Rohan', 'Saha', '2018-06-26', 'Employee', 'Male', '0000-00-00', '', 'Kolkata', 'Kolkata', 'West Bengal', '700001', '9797686796', '', '', '', 'Select', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '0000-00-00', '', '', '', '', '', '', '', '', '', '', 'Select', '', '', '0000-00-00', 'construction', NULL, NULL, NULL),
(4, 'Test20', 'Test21', '0000-00-00', 'Employee', 'Male', '0000-00-00', '', 'Kolkata', 'Kolkata - 700059, North 24 parganas, West Bengal', 'West Bengal', '700059', '9999999999', '', 'mail.mail@gmail.com', '', 'Select', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '0000-00-00', '', '', '', '', '', '', '', '', '', '', 'Select', '', '', '0000-00-00', 'construction', NULL, NULL, NULL),
(5, 'Test20', 'Test21', '2018-06-26', 'Employee', 'Male', '0000-00-00', '', 'Kolkata', 'Kolkata - 700059, North 24 parganas, West Bengal', 'West Bengal', '700059', '9999999999', '', 'mail.mail@gmail.com', 'e8a1b72f635c4608d5c9c61b8d65d030.jpg', 'Select', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '0000-00-00', '', '', '', '', '', '', '', '', '', '', 'Select', '', '', '0000-00-00', 'construction', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contractors`
--
ALTER TABLE `contractors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contractors`
--
ALTER TABLE `contractors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
