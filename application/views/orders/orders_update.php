<!DOCTYPE html>
<html lang="en">

<head>


    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Update Category</title>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url()?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url()?>css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top" ng-app="myApp" ng-controller="myCtrl" ng-cloak>

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?= $menu ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?= $header ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Content Row -->
                    <div class="row">

                        <div class="col-md-8">
                            <div class="card shadow mb-4">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a
                                                href="<?= base_url()?>index.php/Dashboard/orders_view">View</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Update</li>
                                    </ol>
                                </nav>
                                <div class="card-body">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">Product Category</th>
                                                <th scope="col">Product Name</th>
                                                <th scope="col">Quantity</th>
                                                <th scope="col">Unit Price</th>
                                                <th scope="col">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="product in order.products" ng-click="">
                                                <th scope="row">{{product.category.name}}</th>
                                                <td>
                                                    {{product.product.name}}
                                                </td>
                                                <td>
                                                    {{product.product.quantity}}
                                                </td>
                                                <td>
                                                    {{product.product.final_price | number:2}}
                                                </td>
                                                <td>
                                                    {{product.product.final_price*product.product.quantity | number:2}}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- Card Body -->
                            </div> <!-- Card -->
                        </div> <!-- Col-mmd-8 -->

                    </div>

                    <div class="row">

                        <div class="col-md-8">
                            <select class="form-control mb-4 col-md-6 shadow-lg" ng-model="status_selected">
                                <option value="1">Placed</option>
                                <option value="2">Shipped</option>
                                <option value="3">Delivered</option>
                                <option value="4">Refund Initiated</option>
                                <option value="5">Refund Completed</option>
                            </select>

                            <button type="button" ng-click="go_update()"
                                class="btn btn-primary col-md-3">Update</button>
                            <button type="button" ng-click="go_delete()" class="btn btn-danger col-md-3">Delete</button>
                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2019</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url()?>vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url()?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url()?>vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= base_url()?>js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="<?= base_url()?>vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="<?= base_url()?>js/demo/chart-area-demo.js"></script>
    <script src="<?= base_url()?>js/demo/chart-pie-demo.js"></script>

    <script>
    var app = angular.module('myApp', []);
    app.controller('myCtrl', function($scope, $http) {
        $scope.status_selected = "2"
        $http.get("http://<?= $this->config->item('server')?>/android/index.php/orders/get/<?= $order_id?>")
            .then(function(response) {
                $scope.order = response.data
                console.log($scope.order)
                $scope.status_selected = $scope.order.orders.status_id
            });

        $scope.go_update = function() {
            console.log("sendnig update request")
            payload = {
                status_id: $scope.status_selected
            }
            $http.post(
                    "http://<?= $this->config->item('server')?>/android/index.php/orders/update/<?= $order_id?>",
                    payload)
                .then(function(response) {
                    console.log(response.data)
                });
        }
        $scope.go_delete = function() {
          $http.get("http://<?= $this->config->item('server')?>/android/index.php/orders/delete/<?= $order_id?>")
            .then(function(response) {
                console.log(response.data)
                window.location = "<?= base_url()?>index.php/Dashboard/orders_view"
            });
        }
    });
    </script>


</body>

</html>