<!DOCTYPE html>
<html lang="en">

<head>
    <!--select multiploe bootstrap -->
    <link rel="stylesheet" href="<?= base_url()?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url()?>css/bootstrap-select.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?= base_url()?>js/bootstrap.min.js"></script>
    <script src="<?= base_url()?>js/bootstrap-select.min.js"></script>


    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    
    <script src="<?= base_url()?>ckeditor_4.11.4_full/ckeditor/ckeditor.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Product</title>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url()?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url()?>css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top" ng-app="myApp" ng-controller="myCtrl" ng-cloak>

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?= $menu ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?= $header ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->


                    <!-- Content Row -->
                    <div class="row">
                        <!-- Basic Card Example -->

                        <div class="card shadow mb-4 col-md-12">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Add Product</h6>
                            </div>
                            <div class="card-body">
                                <form action="<?= base_url()?>index.php/product/add" method="post"
                                    enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Prodcut Descritpion</label>
                                            <textarea id="textarea" name="product[description]"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label>Category</label>
                                            <select class="form-control" name="category[]" data-style="btn-default" multiple>
                                                
                                                <option ng-repeat="category in categories" value="{{category.category_id}}">{{category.name}}</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="product_name">Product Name</label>
                                            <input type="text" name="product[name]" class="form-control"
                                                id="product_name" placeholder="Enter name">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="company">Company</label>
                                            <input type="text" class="form-control" name="product[company]" id="company"
                                                placeholder="Enter company">
                                        </div>
                                        <!-- <div class="form-group col-md-3">
                                            <label>Parent Category</label>
                                            <select class="form-control" ng-model="second_level_category_selected"
                                                ng-change="category_change()">
                                                <option ng-repeat="second_level_category in second_level_categories"
                                                    value="{{second_level_category.category_id}}">
                                                    {{second_level_category.name}}</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="category1">Category</label>
                                            <select class="form-control" id="category1" name="category[category_id]">
                                                <option ng-repeat="category in categories"
                                                    value="{{category.category_id}}">{{category.name}}</option>
                                            </select>
                                        </div> -->
                                        
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="discount">Discount</label>
                                            <select class="form-control" name="discount[discount_id]"
                                                ng-model="discount_selected_id" ng-change="set_discount()">
                                                <option ng-repeat="discount in discounts"
                                                    value="{{discount.discount_id}}">{{discount.name}}</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Upload Image</label>
                                            <input type="file" id="image" name="images[]" multiple="">
                                        </div>
                                    </div>

                                    <div class="row" ng-repeat="attribute in attributes">
                                        <div class="form-group col-md-3">
                                            <label for="price">Price</label>
                                            <input type="text" class="form-control" ng-model="current_price"
                                                name="attributes[{{$index}}][price]" id="price" placeholder="Enter price">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Final Price</label>
                                            <input
                                                class="form-control" ng-class="{'badge-success': discount_selected_id==1}" ng-disabled="discount_selected_id!=1" ng-model="final_price" ng-value="(current_price-(discount_selected.discount_percentage*0.01*current_price))+(tax_gst*(current_price-(discount_selected.discount_percentage*0.01*current_price))) | number:2">
                                            <input type="hidden" ng-if="!final_price" name="attributes[{{$index}}][final_price]"
                                             ng-value="(current_price-(discount_selected.discount_percentage*0.01*current_price))+(tax_gst*(current_price-(discount_selected.discount_percentage*0.01*current_price))) | number:2">
                                            <input type="hidden" ng-if="final_price" name="attributes[{{$index}}][final_price]"
                                            ng-value="final_price">
                                        </div>                                
                                        <div class="form-group col-md-3">
                                            <label for="tax">Tax</label>
                                            <select class="form-control" id="tax" name="attributes[{{$index}}][tax]" ng-model="tax_gst">
                                                <option value="0">Select Tax</option>
                                                <option ng-repeat="tax in taxes" value="{{tax.gst}}">{{tax.name}} ({{tax.gst}})</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="size">Size</label>
                                            <select class="form-control" id="size" name="attributes[{{$index}}][size]">
                                                <option ng-repeat="size in sizes">{{size}}</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="length">Length(cm)</label>
                                            <input type="text" name="attributes[{{$index}}][length]" class="form-control"
                                                id="length" placeholder="Enter length">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="color">Color</label>
                                            <input type="text" class="form-control" name="attributes[{{$index}}][color] id="color"
                                                placeholder="Enter color">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="quantity">Quantity</label>
                                            <input type="text" class="form-control" name="attributes[{{$index}}][quantity]"
                                                id="quantity" placeholder="Enter quantity">
                                        </div>   
                                        <div class="col-md-1" ng-if="$index != 0">
                                            <label>&nbsp;</label>
                                            <button type="button" class="btn btn-danger" id="remove_button" ng-click="remove_row($index)">Remove</button>
                                                <br/>
                                        </div>                               
                                    </div>
                                    
                                    <div class="row">
                                      <div class="col-md-2">                                       
                                          <button type="button" class="btn btn-info" id="add_row" ng-click="add_row()">Add Row</button>
                                      </div>                                  
                                      <div class="col-md-10">
                                          <button type="submit" class="btn btn-primary" style="float:right;">Submit</button>
                                      </div>
                                    </div>
                                </form>
                                <script>
                                    CKEDITOR.replace('textarea')
                                </script>
                            </div>
                        </div>



                        <!-- Content Row -->

                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Your Website 2019</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="<?= base_url()?>vendor/jquery/jquery.min.js"></script>
        <script src="<?= base_url()?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="<?= base_url()?>vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="<?= base_url()?>js/sb-admin-2.min.js"></script>

        <!-- Page level plugins -->
        <script src="<?= base_url()?>vendor/chart.js/Chart.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="<?= base_url()?>js/demo/chart-area-demo.js"></script>
        <script src="<?= base_url()?>js/demo/chart-pie-demo.js"></script>

        <!-- <script>
            $(document).ready(function () {
                $('.selectpicker').selectpicker();
            })
        </script> -->

        <script>
        var app = angular.module('myApp', []);
        app.controller('myCtrl', function($scope, $http) {
            $scope.tax_gst = "0"

            $http.get("http://<?= $this->config->item('server')?>/android/index.php/category/all")
            .then(function(response) {
                $scope.categories = response.data
                console.log($scope.categories)
            
                $scope.attributes = [1]
                $scope.add_row = function() {
                $scope.attributes.push($scope.attributes[$scope.attributes.length-1]+1)
                }
                $scope.remove_row = function($index) {
                $scope.attributes.splice($index,1)
                }
                $http.get("http://<?= $this->config->item('server')?>/android/index.php/discount/all")
                    .then(function(response) {
                        $scope.discounts = response.data
                        console.log("$scope.discounts")
                        console.log($scope.discounts)
                        $scope.discount_selected_id = $scope.discounts[0].discount_id
                        $scope.current_price = 0
                        $scope.discount_selected = angular.copy($scope.discounts[0])
                    });
                $scope.set_discount = function() {
                    $http.get("http://<?= $this->config->item('server')?>/android/index.php/discount/get/" +
                        $scope.discount_selected_id).then(function(response) {
                        console.log(response.data)
                        data = response.data
                        $scope.discount_selected = angular.copy(data)
                    })
                }

                $scope.sizes = ["S", "M","L", "XL", "XXL", "Not Sure"]

                $http.get("http://<?= $this->config->item('server')?>/android/index.php/tax/all").then(response=>{
                    $scope.taxes = response.data
                })
            });
            

            // $http.get(`http://<?= $this->config->item('server')?>/android/index.php/second_level_category/all`)
            //     .then(function(resp) {
            //         $scope.second_level_categories = resp.data;
            //         console.log("$scope.second_level_categories is");
            //         console.log($scope.second_level_categories);
            //         $scope.second_level_category_selected = $scope.second_level_categories[0].category_id
            //         $http.get(
            //             `http://<?= $this->config->item('server')?>/android/index.php/second_level_category/sub_categories/${$scope.second_level_category_selected}`
            //         ).then(function(resp) {
            //             $scope.categories = resp.data

            //         })

            //     })
            // $scope.category_change = function() {
            //     console.log($scope.second_level_category_selected)
            //     $http.get(
            //         `http://<?= $this->config->item('server')?>/android/index.php/second_level_category/sub_categories/${$scope.second_level_category_selected}`
            //     ).then(function(resp) {
            //         $scope.categories = resp.data
            //         console.log(resp.data)
            //     })
            // }
        });
        </script>


</body>

</html>