<!DOCTYPE html>
<html lang="en">

<head>

    <!--select multiploe bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Update Product</title>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url()?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url()?>css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top" ng-app="myApp" ng-controller="myCtrl" ng-cloak>

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?= $menu ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?= $header ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->


                    <!-- Content Row -->
                    <div class="row">
                        <!-- Basic Card Example -->
                        <div class="col-md-12">
                            <div class="card shadow mb-4">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a
                                                href="<?= base_url()?>index.php/Dashboard/product_view">View</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Update</li>
                                    </ol>
                                </nav>

                                <div class="card-body">
                                    <form name="form1"
                                        action="{{'<?= base_url()?>index.php/product/update/' + product_selected.product.product_id + '/' + product_selected_index}}"
                                        method="post" enctype="multipart/form-data">
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <a href="<?= base_url() ?>uploads/{{product_selected.images[0].image_id}}"
                                                    target="_blank"><img
                                                        src="<?= base_url() ?>uploads/{{product_selected.images[0].image_id}}"
                                                        height="100px" width="100px"></a>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                           <div class="form-group col-md-3">
                                                <label>Category</label>
                                                <!-- <select class="form-control" id="category1" name="category[category_id]"
                                                    ng-model="product_selected.category.category_id">
                                                    <option ng-repeat="category in categories"
                                                        value="{{category.category_id}}">{{category.name}}</option>
                                                </select> -->
                                                <select class="form-control" name="category[]" ng-model="categories_selected" data-style="btn-default" multiple>
                                                    <option ng-repeat="category in categories" value="{{category.category_id}}">{{category.name}}</option>                                                
                                              </select>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="product_name">Product Name</label>
                                                <input class="form-control" id="product_name" name="product[name]"
                                                    ng-model="product_selected.product.name">
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="company">Company</label>
                                                <input type="text" class="form-control" id="company"
                                                    name="product[company]" ng-model="product_selected.product.company">
                                            </div>
                                            <!-- <div class="form-group col-md-3">
                                                <label>Parent Category</label>
                                                <select class="form-control" ng-model="second_level_category_selected"
                                                    ng-change="category_change()">
                                                    <option ng-repeat="second_level_category in second_level_categories"
                                                        value="{{second_level_category.category_id}}">
                                                        {{second_level_category.name}}</option>
                                                </select>
                                            </div>                                         -->
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-3">
                                                <label for="price">Discount</label> 
                                                <select class="form-control" ng-change="set_discount()"
                                                    ng-model="product_selected.discount.discount_id"
                                                    name="discount[discount_id]">
                                                    <option ng-repeat="discount in discounts"
                                                        value="{{discount.discount_id}}">{{discount.name}}</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label>Upload Image</label>
                                                <input type="file" id="image" name="images[]" multiple="">
                                            </div>
                                        </div>
                                        <div class="row" ng-repeat="attribute in attributes">
                                            <div class="form-group col-md-3">
                                                <label for="price">Price</label>
                                                <input type="text" class="form-control" id="price" name="attributes[{{$index}}][price]"
                                                    ng-model="attribute.price">
                                            </div>                                            
                                            <div class="form-group col-md-3">
                                                <label>Final Price</label>
                                                <input
                                                    class="form-control badge-success"  ng-if="product_selected.discount.discount_id==1" ng-model="$parent.final_price" ng-value="product_selected.attributes[$index].final_price">
                                                    <input
                                                    class="form-control" ng-if="product_selected.discount.discount_id!=1" ng-disabled="product_selected.discount.discount_id!=1" ng-value="((attribute.price-(product_selected.discount.discount_percentage*0.01*attribute.price))+attribute.tax*(attribute.price-(product_selected.discount.discount_percentage*0.01*attribute.price)) | number:2) || attribute.price">
                                                <input name="attributes[{{$index}}][final_price]" type="hidden"
                                                ng-if="final_price" ng-value="final_price" >

                                                <input name="attributes[{{$index}}][final_price]" type="hidden"
                                                ng-if="product_selected.discount.discount_id!=1" ng-value="((attribute.price-(product_selected.discount.discount_percentage*0.01*attribute.price))+attribute.tax*(attribute.price-(product_selected.discount.discount_percentage*0.01*attribute.price)) | number:2) || attribute.price" >
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="tax">Tax</label>
                                                <select class="form-control" id="tax" name="attributes[{{$index}}][tax]"
                                                    ng-model="attribute.tax">
                                                    <option value="0">Select Tax</option>
                                                    <option ng-repeat="tax in taxes" value="{{tax.gst}}">{{tax.name}} ({{tax.gst}})</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <input name="attributes[{{$index}}][product_attribute_id]" type="hidden" ng-value="attribute.product_attribute_id">
                                                <input name="attributes[{{$index}}][product_id]" type="hidden" ng-value="attribute.product_id">
                                                <label for="size">Size</label>
                                                <select class="form-control" id="size" name="attributes[{{$index}}][size]"
                                                    ng-model="attribute.size">
                                                    <option ng-repeat="size in sizes">{{size}}</option>
                                                </select>
                                            </div>
                                             <div class="form-group col-md-3">
                                                <label for="length">Length(cm)</label>
                                                <input type="text" class="form-control" id="length"
                                                    name="attributes[{{$index}}][length]"
                                                    value="{{attribute.length}}">
                                            </div>
                                            
                                            <div class="form-group col-md-3">
                                                <label for="color">Color</label>
                                                <input class="form-control" type="text" id="color"
                                                    name="attributes[{{$index}}][color]"
                                                    ng-model="attribute.color">
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="quantity">Quantity</label>
                                                <input type="text" class="form-control" id="quantity"
                                                    name="attributes[{{$index}}][quantity]"
                                                    ng-model="attribute.quantity">
                                            </div>
                                            <div class="col-md-1" ng-if="$index != 0">
                                                <label>&nbsp;</label>
                                                <button type="button" class="btn btn-danger" id="remove_button" ng-click="remove_row($index)">Remove</button>
                                                    <br/>
                                            </div> 
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">                                       
                                                <button type="button" class="btn btn-info" id="add_row" ng-click="add_row()">Add Row</button>
                                            </div>  
                                            <div class="col-md-1">
                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </div>
                                            <div class="col-md-9">
                                                <button type="button"
                                                    ng-click="delete(product_selected.product.product_id)"
                                                    class="btn btn-danger" style="float:right;">Delete Item</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>



                        <!-- Content Row -->

                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Your Website 2019</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="<?= base_url()?>vendor/jquery/jquery.min.js"></script>
        <script src="<?= base_url()?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="<?= base_url()?>vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="<?= base_url()?>js/sb-admin-2.min.js"></script>

        <!-- Page level plugins -->
        <script src="<?= base_url()?>vendor/chart.js/Chart.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="<?= base_url()?>js/demo/chart-area-demo.js"></script>
        <script src="<?= base_url()?>js/demo/chart-pie-demo.js"></script>

        <script>
        var app = angular.module('myApp', []);
        app.controller('myCtrl', function($scope, $http) {
            $scope.attributes = []
            $scope.add_row = function() {
              $scope.attributes.push([])
            }
            $scope.remove_row = function($index) {
              $scope.attributes.splice($index,1)
            }
            $http.get("http://<?= $this->config->item('server')?>/android/index.php/category/all")
                    .then(function(response) {
                        $scope.categories = response.data
                        console.log($scope.categories)
                    });
            $http.get(
                    "http://<?= $this->config->item('server')?>/android/index.php/product/get/<?= $product_id_update?>"
                )
                .then(function(response) {
                    $scope.product_selected = response.data
                    console.log(response.data)
                    angular.copy($scope.product_selected.attributes,$scope.attributes)
                    $scope.categories_selected = []
                    $scope.product_selected.category.map(cat=>{
                        $scope.categories_selected.push(cat.category_id)
                        console.log("$scope.categories_selected")
                        console.log($scope.categories_selected)
                    })
                    
                    // console.log("$scope.product_selected")
                    // console.log($scope.product_selected)
                    // console.log("$scope.attributes")
                    // console.log($scope.attributes)
                    // $http.get(`http://<?= $this->config->item('server')?>/android/index.php/second_level_category/all`)
                    // .then(function(resp) {
                    //     $scope.second_level_categories = resp.data;
                    //     console.log("$scope.second_level_categories is");
                    //     console.log($scope.second_level_categories);
                    //     $scope.second_level_category_selected = $scope.product_selected.category.second_level_category_id
                    //     $http.get(
                    //         `http://<?= $this->config->item('server')?>/android/index.php/second_level_category/sub_categories/${$scope.second_level_category_selected}`
                    //     ).then(function(resp) {
                    //         $scope.categories = resp.data
                            
                    //     })

                    // })
                });
            // $http.get("http://<?= $this->config->item('server')?>/android/index.php/category/all")
            //     .then(function(response) {
            //         $scope.categories = response.data
            //         console.log("$scope.categories")
            //         console.log($scope.categories)
            //     });
            // $scope.set_product = function() {
            //   $scope.form1 = {}
            //   $scope.product_selected = angular.copy($scope.products[$scope.product_selected_index])
            // }
            $http.get("http://<?= $this->config->item('server')?>/android/index.php/discount/all").then(
                function(response) {
                    $scope.discounts = response.data
                })
            $scope.set_discount = function() {
                $http.get("http://<?= $this->config->item('server')?>/android/index.php/discount/get/" +
                    $scope.product_selected.discount.discount_id).then(function(response) {
                    $scope.product_selected.discount = angular.copy(response.data)
                })
            }
            $scope.sizes = ["S","M","L", "XL", "XXL", "Not Sure"]
            $http.get("http://<?= $this->config->item('server')?>/android/index.php/tax/all").then(response=>{
                $scope.taxes = response.data
            })
            $scope.delete = function(product_id) {
                $http.get(`<?= base_url()?>index.php/product/delete/${product_id}`).then(function(
                    response) {
                    window.location = "<?= base_url()?>index.php/Dashboard/product_view"
                })
            }
            
            // $scope.category_change = function() {
            //     console.log($scope.second_level_category_selected)
            //     $http.get(
            //         `http://<?= $this->config->item('server')?>/android/index.php/second_level_category/sub_categories/${$scope.second_level_category_selected}`
            //     ).then(function(resp) {
            //         $scope.categories = resp.data
            //         console.log(resp.data)
            //     })
            // }
        });
        </script>


</body>

</html>