<!DOCTYPE html>
<html lang="en">

<head>


    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Viiew Product</title>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url()?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url()?>css/sb-admin-2.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">


</head>

<body id="page-top" ng-app="myApp" ng-controller="myCtrl" ng-cloak>

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?= $menu ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?= $header ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->


                    <!-- Content Row -->
                    <div class="row">

                        <!--Card-->
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label>Category</label>
                                        <select class="form-control" ng-model="category_selected"
                                            ng-change="filter_products()">
                                            <option value="">Select Category</option>
                                            <option ng-repeat="category in categories" value="{{category.category_id}}">
                                                {{category.name}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Offer</label>
                                        <select class="form-control" ng-model="discount_selected"
                                            ng-change="filter_products()">
                                            <option value="">Select Offer</option>
                                            <option ng-repeat="discount in discounts" value="{{discount.discount_id}}">
                                                {{discount.name}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Sort By</label>
                                        <select class="form-control" ng-model="sort_column_selected"
                                            ng-change="filter_products()">
                                            <option value="-1">Select Sorting Option</option>
                                            <option ng-repeat="column in columns" value="{{column.code}}">
                                                {{column.title}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Sorting Method</label>
                                        <select class="form-control" ng-model="sort_method_selected"
                                            ng-change="filter_products()">
                                            <option value="0">Low to High</option>
                                            <option value="1">High to Low</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Image</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Company</th>
                                            <th scope="col">Category</th>
                                            <th scope="col">Offer</th>    
                                            <th scope="col">Size</th>
                                            <th scope="col">Quantity</th>
                                            <th scope="col">Price</th>            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="obj in products">
                                            <th scope="row">{{$index+1}}</th>
                                            <td><img src="<?= base_url() ?>uploads/{{obj.images[0].image_id}}"
                                                    height="50px" width="50px" ng-click="go_update(obj.product.product_id)"></td>
                                            <td ng-click="go_update(obj.product.product_id)">{{obj.product.name}}</td>
                                            <td ng-click="go_update(obj.product.product_id)">{{obj.product.company}}</td>
                                            <td ng-click="go_update(obj.product.product_id)">{{obj.category[0].name}}</td>
                                            <td ng-click="go_update(obj.product.product_id)">{{obj.discount.name}}</td>                                           
                                            <td>
                                              <select class="form-control shadow-sm" ng-model="attribute_selected[$index]">
                                                <option ng-repeat="attribute in obj.attributes" ng-value="attribute">{{attribute.size}}</option>
                                              </select>
                                            </td>
                                            <td><label>{{attribute_selected[$index].quantity}}</label></td>
                                            <td><label><del>{{attribute_selected[$index].price}}</del>&nbsp;{{attribute_selected[$index].final_price}}</label></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div> <!-- /row -->
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2019</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>




    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url()?>vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url()?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url()?>vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= base_url()?>js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="<?= base_url()?>vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="<?= base_url()?>js/demo/chart-area-demo.js"></script>
    <script src="<?= base_url()?>js/demo/chart-pie-demo.js"></script>

    <script>
    var app = angular.module('myApp', []);
    app.controller('myCtrl', function($scope, $http) {
        $scope.category_selected = ""
        $scope.discount_selected = ""
        $scope.sort_column_selected = "-1"
        $scope.sort_method_selected = "0"
        $scope.menu_toggled = false
        
        $http.get("http://<?= $this->config->item('server')?>/android/index.php/product/all")
            .then(function(response) {
                $scope.products = response.data
                //var product
                $scope.attribute_selected = []
                $scope.products.map(product=>{
                  if(product.attributes[0] != undefined) {
                    $scope.attribute_selected.push(product.attributes[0])
                  }
                  else {
                    $scope.attribute_selected.push(0)
                  }
                })
                console.log($scope.products)
            });
        $http.get("http://<?= $this->config->item('server')?>/android/index.php/category/all")
            .then(function(response) {
                $scope.categories = response.data
            });
        $http.get("http://<?= $this->config->item('server')?>/android/index.php/discount/all").then(function(
            response) {
            $scope.discounts = response.data
        })
        $scope.go_update = function(product_id, index) {
            window.location = `<?= base_url()?>index.php/product/update/${product_id}`
        }
        $scope.columns = [{
                "title": "Name",
                "code": "name"
            },
            // {
            //     "title": "Quantity",
            //     "code": "quantity"
            // },
            {
                "title": "Price",
                "code": "-2"
            },
            {
                "title": "Company",
                "code": "company"
            }
            // ,{
            //     "title": "Tax",
            //     "code": "tax"
            // }
        ]

        $scope.filter_products = function() {
            var sort_by_price = function() {
                $scope.products = $scope.products.sort((a,b)=>{
                  if(a.attributes[0] != undefined && b.attributes[0] != undefined) {
                    if($scope.sort_method_selected == 0) {
                      return(parseInt(a.attributes[0].final_price)-parseInt(b.attributes[0].final_price))
                    }
                    else {
                      return parseInt(b.attributes[0].final_price)-parseInt(a.attributes[0].final_price)
                    }
                  }
                })
            }
            var set_attribute_selected = function() {
                $scope.attribute_selected = []
                $scope.products.map(product=>{
                  if(product.attributes[0] != undefined) {
                    $scope.attribute_selected.push(product.attributes[0])
                  }
                  else {
                    $scope.attribute_selected.push(0)
                  }
                })
            }
            
            if ($scope.category_selected == "" && $scope.discount_selected == "") {
                $http.get(
                    `http://<?= $this->config->item('server')?>/android/index.php/product/all/${$scope.sort_column_selected}/${$scope.sort_method_selected}`
                )
                .then(function(response) {
                    $scope.products = response.data  
                    if($scope.sort_column_selected == "-2") {
                        sort_by_price()
                    }
                    set_attribute_selected()
                });
                
            } else if ($scope.category_selected != "" && $scope.discount_selected == "") {
                
                $http.get(
                    `http://<?= $this->config->item('server')?>/android/index.php/product/product_by_category/${$scope.category_selected}/${$scope.sort_column_selected}/${$scope.sort_method_selected}`
                ).then(function(response) {
                    $scope.products = response.data
                    if($scope.sort_column_selected == "-2") {
                        sort_by_price()
                    }
                    set_attribute_selected()
                })
                
                
            } else if ($scope.category_selected == "" && $scope.discount_selected != "") {
                $http.get(
                    `http://<?= $this->config->item('server')?>/android/index.php/product/product_by_discount/${$scope.discount_selected}/${$scope.sort_column_selected}/${$scope.sort_method_selected}`
                ).then(function(response) {
                    $scope.products = response.data
                    if($scope.sort_column_selected == "-2") {
                        sort_by_price()
                    }
                    set_attribute_selected()
                })
                
            } else {
                $http.get(
                    `http://<?= $this->config->item('server')?>/android/index.php/product/product_by_discount_category/${$scope.discount_selected}/${$scope.category_selected}/${$scope.sort_column_selected}/${$scope.sort_method_selected}`
                ).then(function(response) {
                    $scope.products = response.data
                    if($scope.sort_column_selected == "-2") {
                        sort_by_price()
                    }
                    set_attribute_selected()
                })
            }
        }
    });
    </script>


</body>

</html>