<!DOCTYPE html>
<html lang="en">

<head>

  
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url()?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url()?>css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top" ng-app="myApp" ng-controller="myCtrl" ng-cloak>

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?= $menu ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?= $header ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          

          <!-- Content Row -->
          <div class="row" ng-repeat="product in products">
            <div class="col-md-3"> <!--ng-repeat="obj in product"-->
              <!--Card-->
              <div class="card card-cascade card-ecommerce wider h-100">
                  <!--Card image-->
                  <div class="view view-cascade overlay">
                    <!-- <img class="card-img-top img-thumbnail" height="100px" width="100px" src="<?= base_url() ?>uploads/{{obj.product.image_id}}" alt="" -->
                    <a>
                      <div class="mask rgba-white-slight"></div>
                    </a>
                  </div>
                  <!--/.Card image-->

                  <!--Card content-->
                  <div class="card-body card-body-cascade text-center">
                    <!--Category & Title-->
                    <!-- <h5>{{obj.category.name.length>15?obj.category.name.substring(0,15)+"\.\.\.":obj.category.name}}</h5>
                    <h4 class="card-title sm"><strong><a href=""><font size="4px">{{obj.product.name.length>15?obj.product.name.substring(0,15)+"\.\.\.":obj.product.name}}</font></a></strong></h4> -->

                    <!--Description-->
                    <!-- <p class="sm">{{obj.attributes.color}}</p>
                    <p class="sm">{{obj.attributes.size}}</p> -->
                    <!--Card footer-->
                    <!-- <div class="card-footer">
                      <span class="float-left">
                      {{obj.product.final_price | number:2}}/=
                      </span>
                    </div> -->
                  </div> <!--/.Card content-->                 
              </div> <!--/.Card-->
              
            </div> <!-- col-md-4 -->
          </div> <!-- /row -->
        </div>  
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  
  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()?>vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()?>vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()?>js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?= base_url()?>vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url()?>js/demo/chart-area-demo.js"></script>
  <script src="<?= base_url()?>js/demo/chart-pie-demo.js"></script>

  <script>
    var app = angular.module('myApp', []);
    app.controller('myCtrl', function($scope, $http) {
      // $http.get("http://<?= $this->config->item('server')?>/android/index.php/product/all")
      // .then(function(response) {
      //   var products_raw = response.data
      //   $scope.products = []
      //   while(products_raw.length) $scope.products.push(products_raw.splice(0,4));
      //   console.log("$scope.products")
      //   console.log($scope.products)
      // });
    });
  </script>
  

</body>

</html>
