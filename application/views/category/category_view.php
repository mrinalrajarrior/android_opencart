<!DOCTYPE html>
<html lang="en">

<head>
  
  
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Update Category</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url()?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url()?>css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top" ng-app="myApp" ng-controller="myCtrl" ng-cloak>

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?= $menu ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?= $header ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          

          <!-- Content Row -->
          <div class="row">
          <!-- Basic Card Example -->
            
            <div class="card shadow mb-4 col-md-8">     
                  <div class="card-body">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th scope="col">#</th>        
                          <th scope="col">Name</th>
                          <th scope="col">Number of Items</th>                                              
                        </tr>
                      </thead>
                      <tbody>
                        <tr ng-repeat="category in categories" ng-click="go_update(category.category_id,$index)">
                          <th scope="row">{{$index+1}}</th>
                          <td>{{category.name}}</td>
                          <td>
                            {{get_quantity(category)}}
                          </td>
                        </tr>
                      </tbody>
                    </table>    
                  </div>
            </div>&nbsp;
            
           

          <!-- Content Row -->

        </div>  
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()?>vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()?>vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()?>js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?= base_url()?>vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url()?>js/demo/chart-area-demo.js"></script>
  <script src="<?= base_url()?>js/demo/chart-pie-demo.js"></script>

  <script>
    var app = angular.module('myApp', []);
    app.controller('myCtrl', function($scope, $http) {
      $http.get("http://<?= $this->config->item('server')?>/android/index.php/category/all")
      .then(function(response) {
        $scope.categories = response.data
        $scope.category_selected_index = "<?= isset($selected_index)?$selected_index:0?>"
        $scope.category_selected = angular.copy($scope.categories[$scope.category_selected_index])
      });
      $scope.set_category = function() {
        $scope.category_selected = angular.copy($scope.categories[$scope.category_selected_index])
      }
      $scope.category_items = []
      $http.get("http://<?= $this->config->item('server')?>/android/index.php/category/get_number_of_items").then(function(response) {
        $scope.category_items = response.data
      })
      $scope.get_quantity = function(category) {
        var obj = $scope.category_items.find(function(obj) {
          if(obj.category_id == category.category_id) {
            return obj;
          }
        })
        return (obj?obj.number_items:0);
      }
      $scope.go_update = function(category_id,idx) {
        window.location = `<?= base_url()?>index.php/category/update/${category_id}/${idx}`
      }
    });
  </script>
  

</body>

</html>
