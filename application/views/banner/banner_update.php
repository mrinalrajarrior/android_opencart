<!DOCTYPE html>
<html lang="en">

<head>


    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Update Banner</title>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url()?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url()?>css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top" ng-app="myApp" ng-controller="myCtrl" ng-cloak>

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?= $menu ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?= $header ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->


                    <!-- Content Row -->
                    <div class="row">
                        <!-- Basic Card Example -->
                        <div class="col-md-12">
                            <div class="card shadow mb-4">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a
                                                href="<?= base_url()?>index.php/Dashboard/banner_view">View</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Update</li>
                                    </ol>
                                </nav>

                                <div class="card-body">
                                    <form name="form1"
                                        action=""
                                        method="post" enctype="multipart/form-data">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Banner Name</label>
                                                <input type="text" class="form-control" ng-model="banner.name">
                                            </div>
                                        </div>
                                        <div class="row"  ng-repeat="offer in banner.offers">
                                           
                                            <div class="form-group col-md-4">
                                                <a href="<?= base_url() ?>uploads/{{offer.image_id}}"
                                                    target="_blank"><img
                                                        src="<?= base_url() ?>uploads/{{offer.image_id}}"
                                                        height="150px" width="200px" class="shadow-sm"></a>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <button type="button" class="form-control btn btn-danger shadow-sm" ng-click="remove_image($index)">Remove</button>
                                            </div>
                                            <br/>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <input type="button" value="Submit" ng-click="go_update()" class="btn btn-primary shadow-sm">
                                            </div>
                                            <div class="col-md-8">
                                                <!-- Placeholder -->
                                            </div>
                                            <div class="col-md-2">
                                                <input type="button" value="Delete" ng-click="go_delete()" style="float:right" class="btn btn-danger shadow-sm">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>



                        <!-- Content Row -->

                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Your Website 2019</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>
        <!-- Bootstrap core JavaScript-->
        <script src="<?= base_url()?>vendor/jquery/jquery.min.js"></script>
        <script src="<?= base_url()?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="<?= base_url()?>vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="<?= base_url()?>js/sb-admin-2.min.js"></script>

        <!-- Page level plugins -->
        <script src="<?= base_url()?>vendor/chart.js/Chart.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="<?= base_url()?>js/demo/chart-area-demo.js"></script>
        <script src="<?= base_url()?>js/demo/chart-pie-demo.js"></script>

        <script>
        var app = angular.module('myApp', []);
        app.controller('myCtrl', function($scope, $http) {
            $http.get("http://<?= $this->config->item('server')?>/android/index.php/banner/get/<?= $banner_id?>")
            .then(response=>{
                $scope.banner = response.data
                console.log($scope.banner)
            });
            $scope.remove_image = function($index) {
                $scope.banner.offers.splice($index,1)
            }
            $scope.go_update = function() {
                console.log("inside go update")
                $http.post('http://<?= $this->config->item('server')?>/android/index.php/banner/update_api', $scope.banner).then(response=>{
                    window.location.reload(true)
                })
            }
            $scope.go_delete = function() {
                window.location = `http://<?= $this->config->item('server') ?>/android/index.php/banner/delete/${$scope.banner.banner_id}`
            }
        });
        </script>


</body>

</html>