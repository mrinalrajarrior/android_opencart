<!DOCTYPE html>
<html lang="en">

<head>

  
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Update Offer</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url()?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url()?>css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top" ng-app="myApp" ng-controller="myCtrl" ng-cloak>

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?= $menu ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?= $header ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          

          <!-- Content Row -->
          <div class="row">
          <!-- Basic Card Example -->
            
            <div class="card shadow mb-4 col-md-8">
                  <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Update Offer</h6>
                  </div>
                  <div class="card-body">
                    <form name="form1" action="{{'<?= base_url()?>index.php/discount/update/' + discount_selected.discount_id + '/' + discount_selected_index}}" method="post" enctype="multipart/form-data">
                      <div class="row">
                        <div class="form-group col-md-5">
                          <label for="offer_name_hard">Offer Name</label>
                          <select class="form-control" id="offer_name_hard" ng-change="set_discount()" ng-model="discount_selected_index">
                            <option ng-repeat="discount in discounts" value="{{$index}}">{{discount.name}}</option>
                          </select>
                        </div>&nbsp
                      </div>

                      <div class="row">
                        <div class="form-group col-md-5">
                          <label for="offer_name">Offer Name</label>
                          <input class="form-control" id="offer_name" name="name" ng-model="discount_selected.name">
                        </div>&nbsp
                        <div class="form-group col-md-5">
                          <label for="discount_percentage">Discount Percentage</label>
                          <input class="form-control" id="discount_percentage" name="discount_percentage" ng-model="discount_selected.discount_percentage">
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-12">
                          <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                      </div>
                    </form>    
                  </div>
            </div>&nbsp;
            
           

          <!-- Content Row -->

        </div>  
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()?>vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()?>vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()?>js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?= base_url()?>vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url()?>js/demo/chart-area-demo.js"></script>
  <script src="<?= base_url()?>js/demo/chart-pie-demo.js"></script>

  <script>
    var app = angular.module('myApp', []);
    app.controller('myCtrl', function($scope, $http) {
      $http.get("http://<?= $this->config->item('server')?>/android/index.php/discount/all")
      .then(function(response) {
        $scope.discounts = response.data
        console.log("$scope.discounts")
        console.log($scope.discounts)
        console.log("selected_index")
        $scope.discount_selected_index = "<?= isset($selected_index)?$selected_index:0 ?>"
        
        $scope.discount_selected = angular.copy($scope.discounts[$scope.discount_selected_index])
      });
      $scope.set_discount = function() {
        $scope.discount_selected = angular.copy($scope.discounts[$scope.discount_selected_index])
      }
    });
  </script>
  

</body>

</html>
