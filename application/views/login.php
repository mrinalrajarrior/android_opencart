<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?= $this->config->item('title') ?> | Login</title>

    <link href="<?= base_url() ?>inspinia/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?= base_url() ?>inspinia/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>inspinia/css/animate.css" rel="stylesheet">
    <link href="<?= base_url() ?>inspinia/css/style.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.3.0/css/iziToast.min.css" rel="stylesheet">


</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div id="login-box">

            <h1 class="logo-name"><?= $this->config->item('company_nickname') ?></h1>

        </div>
        <h3>Welcome to <?= $this->config->item('company') ?></h3>
        <p><?= $this->config->item('company_about') ?></p>
        <form class="m-t" role="form" method="post" id="login_form">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Username" required="" name="username" autocomplete="off">
            </div>
            <div class="form-group">
                <input type="password" id="password" class="form-control" placeholder="Password" required="" name="password">
            </div>
            <button type="submit" id="submit-btn" class="btn btn-success full-width m-b">Login</button>

            <!-- <a href="<?= base_url() ?>index.php/login/forgot"><small>Forgot password?</small></a> -->
            <p class="text-muted text-center"><small><?= $this->config->item('footnote') ?></small></p>
        </form>
    </div>
</div>

<!-- Mainly scripts -->
<script src="<?= base_url() ?>inspinia/js/jquery-3.1.1.min.js"></script>
<script src="<?= base_url() ?>inspinia/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?= base_url() ?>inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?= base_url() ?>inspinia/js/inspinia.js"></script>
<script src="<?= base_url() ?>inspinia/js/plugins/pace/pace.min.js"></script>
<script src="<?= base_url() ?>inspinia/js/plugins/toastr/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.3.0/js/iziToast.min.js"></script>
<script>

    $(function(){
        $('#login_form').on('submit', function(e){
            e.preventDefault();
            $("#submit-btn").attr('disabled','disabled');
            $("#submit-btn").text('Logging in');
            $.post("<?= base_url() ?>index.php/login", $("#login_form").serialize(), function(data) {
                console.log(data);
                if(data==0){
                    iziToast.error({
                        title: '!',
                        message: 'Invalid credentials!',
                        progressBar: false,
                        timeout: 1500,
                        position: 'bottomCenter'
                    });
                    $("#submit-btn").removeAttr('disabled',);
                    $("#submit-btn").text('Login');
                }else {
                    iziToast.success({
                        title: '!',
                        message: 'Login successful!',
                        progressBar: false,
                        timeout: 1500,
                        position: 'bottomCenter'
                    });
                    console.log('Success');
                    window.location = '<?= base_url() ?>index.php/dashboard';
                }
            });
        })

    })

</script>
</body>

</html>
