<?php

    function get_product($id){

        $CI = get_instance();
        
        $prod=(array)$CI->m_product->get($id);
        if($prod == null) {
            return null;
        } 
        $attr = (array)$CI->m_attributes->get($prod["product_id"]);
        $prod_categories = (array)$CI->m_product_category_mapping->get($prod["product_id"]);
        
        $category = [];
        foreach($prod_categories as $prod_category){
            $cat = (array)$CI->m_category->get($prod_category["category_id"]);
            array_push($category,$cat);
        }
        $prod_discount = (array)$CI->m_product_discount_mapping->get($prod["product_id"]);
        $discount = (array)$CI->m_discount->get($prod_discount["discount_id"]);
        $images = $CI->m_image->get_by_product_id($prod["product_id"]);
        $res = array(
            "product" => $prod,
            "images" => $images,
            "attributes" => $attr,
            "category" => $category,
            "discount" => $discount
        );
        return ($res);
    }

    function get_product_without_attributes($id){

        $CI = get_instance();
        
        $prod=(array)$CI->m_product->get($id);
        if($prod == null) {
            return null;
        } 
        $prod_categories = (array)$CI->m_product_category_mapping->get($prod["product_id"]);
        $category = [];
        foreach($prod_categories as $prod_category){
            $cat = (array)$CI->m_category->get($prod_category["category_id"]);
            array_push($category,$cat);
        }
        
        $prod_discount = (array)$CI->m_product_discount_mapping->get($prod["product_id"]);
        $discount = (array)$CI->m_discount->get($prod_discount["discount_id"]);
        $images = $CI->m_image->get_by_product_id($prod["product_id"]);
        $res = array(
            "product" => $prod,
            "images" => $images,
            "category" => $category,
            "discount" => $discount
        );
        return ($res);
    }

    function get_customer($id){
        $CI = get_instance();
        $cust = $CI->m_customer->get($id);
        if(isset($cust)) {
            $addresses = $CI->m_customer_address_mapping->get_addresses($cust->customer_id);
            $res = array(
                "customer"=>$cust,
                "addresses"=>$addresses
            );

            return ($res);
        }
        else {
            return([]);
        }
        
    }