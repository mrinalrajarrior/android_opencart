<?php


class m_order_status extends CI_Model
{
    public function create($data){
        $this->db->insert('order_status',$data);
    }
    public function all(){
        return $this->db->get('order_status')->result();
    }
	public function get($id) {
		return $this->db->where(["status_id"=>$id])->get('order_status')->row();
	}
	public function update($id,$data) {
		$this->db->where("status_id", $id);
		$this->db->update("order_status", $data);
	}
	public function delete($id) {
		$this->db->delete('order_status',['status_id'=>$id]);
	}
}