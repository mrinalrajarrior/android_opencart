<?php


class m_tax extends CI_Model
{
    public function create($data){
        $this->db->insert('tax',$data);
    }
    public function all(){
        return $this->db->get('tax')->result_array();
    }
	public function get($id) {
		return $this->db->where(["tax_id"=>$id])->get('tax')->row();
	}
	public function update($id,$data) {
		$this->db->where("tax_id", $id);
		$this->db->update("tax", $data);
	}
	public function delete($id) {
		$this->db->delete('tax',['tax_id'=>$id]);
	}
	public function get_by_customer($id) {
		return $this->db->where(["customer_id"=>$id])->get('tax')->result_array();
	}
}