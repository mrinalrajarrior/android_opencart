<?php


class m_attributes extends CI_Model
{
    public function create($data){
        $this->db->insert('attributes',$data);
    }
    public function all(){
        return $this->db->get('attributes')->result();
    }
	public function get($id) {
		return $this->db->where(["product_id"=>$id])->get('attributes')->result_array();
	}
	public function update($id,$data) {
		$this->db->where("product_id", $id);
		$this->db->update("attributes", $data);
	}
	public function update_by_product_attribute_id($product_attribute_id,$data) {
		$this->db->where("product_attribute_id", $product_attribute_id);
		$this->db->update("attributes", $data);
	}
	public function delete($id) {
		$this->db->delete('attributes',['product_id'=>$id]);
	}
	public function delete_by_product_attribute_id($id) {
		$this->db->delete('attributes',['product_attribute_id'=>$id]);
	}
	public function get_by_product_attribute_id($product_attribute_id) {
		return $this->db->where(["product_attribute_id"=>$product_attribute_id])->get('attributes')->row();
	}
}