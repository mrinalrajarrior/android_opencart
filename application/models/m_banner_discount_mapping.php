<?php


class m_banner_discount_mapping extends CI_Model
{
    public function create($data){
        $this->db->insert('banner_discount_mapping',$data);
    }
    public function all(){
        return $this->db->get('banner_discount_mapping')->result();
    }
	public function get_by_banner_id($id) {
		return $this->db->where(["banner_id"=>$id])->get('banner_discount_mapping')->result_array();
	}
	public function get_by_discount_id($id) {
		return $this->db->where(["disocunt_id"=>$id])->get('banner_discount_mapping')->result_array();
	}
	public function update($id,$data) {
		$this->db->where("banner_id", $id);
		$this->db->update("banner_discount_mapping", $data);
	}
	public function delete_by_banner_id($id) {
		$this->db->delete('banner_discount_mapping',['banner_id'=>$id]);
	}
}