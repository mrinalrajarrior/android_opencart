<?php


class m_discount extends CI_Model
{
    public function create($data){
        $this->db->insert('discount',$data);
    }
    public function all(){
        return $this->db->get('discount')->result();
    }
	public function get($id) {
		return $this->db->where(["discount_id"=>$id])->get('discount')->row();
	}
	public function update($id,$data) {
		$this->db->where("discount_id", $id);
		$this->db->update("discount", $data);
	}
	public function delete($id) {
		$this->db->delete('discount',['discount_id'=>$id]);
	}
}