<?php


class m_orders extends CI_Model
{
    public function create($data){
        $this->db->insert('orders',$data);
    }
    public function all(){
        return $this->db->get('orders')->result();
    }
	public function get($id) {
		return $this->db->where(["order_id"=>$id])->get('orders')->row();
	}
	public function update($id,$data) {
		$this->db->where("order_id", $id);
		$this->db->update("orders", $data);
	}
	public function delete($id) {
		$this->db->delete('orders',['order_id'=>$id]);
	}
	public function cancel($id) {
		$this->db->where("order_id", $id);
		$this->db->update('orders',array(
			"cancelled" => 1
		));
	}
	public function get_by_customer($id) {
		return $this->db->where(["customer_id"=>$id])->get('orders')->result_array();
	}
}