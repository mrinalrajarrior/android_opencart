<?php


class m_customer_address_mapping extends CI_Model
{
    public function create($data){
        $this->db->insert('customer_address_mapping',$data);
    }
    public function all(){
        return $this->db->get('customer_address_mapping')->result();
    }
	public function get($id) {
		return $this->db->where(["customer_id"=>$id])->get('customer_address_mapping')->row();
	}
	public function update($id,$data) {
		$this->db->where("customer_id", $id);
		$this->db->update("customer_address_mapping", $data);
	}
	public function delete($id) {
		$this->db->delete('customer_address_mapping',['customer_id'=>$id]);
	}
	public function get_addresses($customer_id) {
		$query = $this->db->query('select * from customer_address_mapping join address on customer_address_mapping.address_id = address.address_id where customer_id='.$customer_id);
		return ($query->result());
	}
}