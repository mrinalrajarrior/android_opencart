<?php


class m_product_discount_mapping extends CI_Model
{
    public function create($data){
        $this->db->insert('product_discount_mapping',$data);
    }
    public function all(){
        return $this->db->get('product_discount_mapping')->result();
    }
	public function get($id) {
		return $this->db->where(["product_id"=>$id])->get('product_discount_mapping')->row();
	}
	public function update($id,$data) {
		$this->db->where("product_id", $id);
		$this->db->update("product_discount_mapping", $data);
	}
	public function delete($id) {
		$this->db->delete('product_discount_mapping',['product_id'=>$id]);
	}
}