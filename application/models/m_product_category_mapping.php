<?php


class m_product_category_mapping extends CI_Model
{
    public function create($data){
        $this->db->insert('product_category_mapping',$data);
    }
    public function all(){
        return $this->db->get('product_category_mapping')->result();
    }
	public function get($id) {
		return $this->db->where(["product_id"=>$id])->get('product_category_mapping')->result_array();
	}
	public function update($id,$data) {
		$this->db->where("product_id", $id);
		$this->db->update("product_category_mapping", $data);
	}
	public function delete($id) {
		$this->db->delete('product_category_mapping',['product_id'=>$id]);
	}
}