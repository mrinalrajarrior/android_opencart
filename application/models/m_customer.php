<?php


class m_customer extends CI_Model
{
    public function create($data){
        $this->db->insert('customer',$data);
    }
    public function all(){
        return $this->db->get('customer')->result();
    }
	public function get($id) {
		return $this->db->where(["customer_id"=>$id])->get('customer')->row();
	}
	public function update($id,$data) {
		$this->db->where("customer_id", $id);
		$this->db->update("customer", $data);
	}
	public function delete($id) {
		$this->db->delete('customer',['customer_id'=>$id]);
	}
	public function search_customer($token) {
		return $this->db->where(["token"=>$token])->get('customer')->row();
	}
}