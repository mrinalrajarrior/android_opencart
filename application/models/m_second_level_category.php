<?php


class m_second_level_category extends CI_Model
{
    public function create($data){
        $this->db->insert('second_level_category',$data);
    }
    public function all(){
        return $this->db->get('second_level_category')->result();
    }
	public function get($id) {
		return $this->db->where(["category_id"=>$id])->get('second_level_category')->row();
	}
	public function update($id,$data) {
		$this->db->where("category_id", $id);
		$this->db->update("second_level_category", $data);
	}
	public function delete($id) {
		$this->db->delete('second_level_category',['category_id'=>$id]);
	}
	public function get_number_of_items() {
		$query = $this->db->query('select cat.category_id, cat.name, count(*) as number_items from category as cat join product_category_mapping as catmap on cat.category_id=catmap.category_id group by cat.category_id, cat.name');
		return $query->result();
	}

	public function sub_categories($id) {
		$query = $this->db->query('select * from second_level_category sc join category c on sc.category_id=c.second_level_category_id where sc.category_id='.$id);
		return $query->result();
	}
}