<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 6/2/2018
 * Time: 1:47 PM
 */

class m_user extends  CI_Model
{
    public $table = "userinfo";
    public function login(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $record = $this->db->where("loginname", $username)
            ->where("password", $password)
            ->get($this->table)
            ->row();
        return $record;
    }
    public function getAll(){
        return $this->db->get('userinfo')->result();
    }
    public function getDesignations(){
        $result = $this->db->query('select distinct designation as designation from userinfo where length(designation)>0')->result();
        $designations = array();
        foreach ($result as $row){
            $designations[] = $row->designation;
        }
        return $designations;
    }
    public function getRoles(){
        return array("Admin", "User");
    }
    public function create(){
        $user = $this->input->post();
        $this->db->insert('userinfo', $user);
        $user_id = $this->db->insert_id();
        return $user_id;
    }
    public function getById($id){
        return $this->db->where('id', $id)->get('userinfo')->row();
    }
    public function getDepartments(){
        $query="select distinct department as department from userinfo where length(department)>0";
        $result = $this->db->query($query)->result();
        $departments = [];
        foreach ($result as $row){
            $departments[] = $row->department;
        }
        return $departments;
    }
    public function update($id){
        $this->db->where('id', $id)
            ->update('userinfo', $this->input->post());
        return 1;
    }
}