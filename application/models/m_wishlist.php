
<?php


class m_wishlist extends CI_Model
{
    public function create($data){
        $this->db->insert('wishlist',$data);
    }
    public function all(){
        return $this->db->get('wishlist')->result();
    }
	public function get($id) {
		return $this->db->where(["customer_id"=>$id])->get('wishlist')->result_array();
	}
	public function update($id,$data) {
		$this->db->where("customer_id", $id);
		$this->db->update("wishlist", $data);
	}
	public function delete($customer_id,$product_attribute_id) {
		$this->db->delete('wishlist',['customer_id'=>$customer_id,'product_attribute_id'=>$product_attribute_id]);
	}
}