<?php


class m_cart_product_mapping extends CI_Model
{
    public function create($data){
    	$this->db->where(["cart_id"=>$data['cart_id'], "product_attribute_id"=>$data['product_attribute_id']])->delete('cart_product_mapping');
    	if($data['quantity']>0){
		    $this->db->insert('cart_product_mapping', $data);
	    }
    }
    public function all(){
        return $this->db->get('cart_product_mapping')->result();
    }
	public function get($id) {
		return $this->db->where(["cart_id"=>$id])->get('cart_product_mapping')->result();
	}
	public function update($cart_id,$product_attribute_id,$data) {
		$this->db->where(["cart_id"=>$cart_id,"product_attribute_id"=>$product_attribute_id]);
		$this->db->update("cart_product_mapping", $data);
	}
	public function delete($cart_id,$product_attribute_id) {
		$this->db->delete('cart_product_mapping',['cart_id'=>$cart_id,"product_attribute_id"=>$product_attribute_id]);
		return($this->db->where(["cart_id"=>$cart_id])->get('cart_product_mapping')->row());
	}
	public function update_batch($data, $where_attribute) {
		$this->db->update_batch("cart_product_mapping", $data, $where_attribute);
	}
}