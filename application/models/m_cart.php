<?php


class m_cart extends CI_Model
{
    public function create($data){
        $this->db->insert('cart',$data);
    }
    public function all(){
        return $this->db->get('cart')->result();
    }
	public function get($id) {
		return $this->db->where(["customer_id"=>$id])->get('cart')->row();
	}
	public function update($id,$data) {
		$this->db->where("cart_id", $id);
		$this->db->update("cart", $data);
	}
	public function delete($id) {
		$this->db->delete('cart',['customer_id'=>$id]);
	}
}