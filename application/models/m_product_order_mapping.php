<?php


class m_product_order_mapping extends CI_Model
{
    public function create($data){
        $this->db->insert('product_order_mapping',$data);
    }
    public function all(){
        return $this->db->get('product_order_mapping')->result();
    }
	public function get($id) {
		return $this->db->where(["order_id"=>$id])->get('product_order_mapping')->result();
	}
	public function update($id,$data) {
		$this->db->where("order_id", $id);
		$this->db->update("product_order_mapping", $data);
	}
	public function delete($id) {
		$this->db->delete('product_order_mapping',['order_id'=>$id]);
	}
}