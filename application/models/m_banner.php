<?php


class m_banner extends CI_Model
{
    public function create($data){
        $this->db->insert('banner',$data);
    }
    public function all(){
        return $this->db->get('banner')->result_array();
    }
	public function get($id) {
		return $this->db->where(["banner_id"=>$id])->get('banner')->row();
	}
	public function update($id,$data) {
		$this->db->where("banner_id", $id);
		$this->db->update("banner", $data);
	}
	public function delete($id) {
		$this->db->delete('banner',['banner_id'=>$id]);
    }
    // public function delete_by_product_id($product_id) {
	// 	$this->db->delete('banner',['product_id'=>$product_id]);
    // }
    // public function get_by_product_id($product_id) {
    //     return($this->db->select('banner_id')->where(["product_id"=>$product_id])->get('banner')->result_array());
    // }    
}