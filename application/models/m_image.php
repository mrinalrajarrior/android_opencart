<?php


class m_image extends CI_Model
{
    public function create($data){
        $this->db->insert('image',$data);
    }
    public function all(){
        return $this->db->get('image')->result();
    }
	public function get($id) {
		return $this->db->where(["image_id"=>$id])->get('image')->row();
    }
    public function get_by_banner_id($id) {
		return $this->db->select("image_id,url")->where(["banner_id"=>$id])->get('image')->result_array();
    }
    public function delete_by_banner_id($banner_id) {
		$this->db->delete('image',['banner_id'=>$banner_id]);
    }
	public function update($id,$data) {
		$this->db->where("image_id", $id);
		$this->db->update("image", $data);
	}
	public function delete($id) {
		$this->db->delete('image',['image_id'=>$id]);
    }
    public function delete_by_product_id($product_id) {
		$this->db->delete('image',['product_id'=>$product_id]);
    }
    public function get_by_product_id($product_id) {
        return($this->db->select('image_id')->where(["product_id"=>$product_id])->get('image')->result_array());
    }
}