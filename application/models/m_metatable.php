<?php


class m_metatable extends CI_Model
{
    public function create($data){
        $this->db->insert('metatable',$data);
    }
    public function all(){
        return $this->db->get('metatable')->result();
    }
	public function get($id) {
		return $this->db->where(["id"=>$id])->get('metatable')->row();
	}
	public function update($id,$data) {
		$this->db->where("id", $id);
		$this->db->update("metatable", $data);
	}
	public function delete($id) {
		$this->db->delete('metatable',['id'=>$id]);
	}
}