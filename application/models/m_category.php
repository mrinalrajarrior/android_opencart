<?php


class m_category extends CI_Model
{
    public function create($data){
        $this->db->insert('category',$data);
    }
    public function all(){
    	$this->db->select("category.*, image.image_id");
    	$this->db->join("image", "category.category_id = image.category_id");
        return $this->db->get('category')->result();
    }
	public function get($id) {
    	$this->db->select("category.*, image.image_id");
    	$this->db->join("image", "category.category_id = image.category_id");
		return $this->db->where(["category.category_id"=>$id])->get('category')->row();
	}
	public function update($id,$data) {
		$this->db->where("category_id", $id);
		$this->db->update("category", $data);
	}
	public function delete($id) {
		$this->db->delete('category',['category_id'=>$id]);
	}
	public function get_number_of_items() {
		$query = $this->db->query('select cat.category_id, cat.name, count(*) as number_items from category as cat join product_category_mapping as catmap on cat.category_id=catmap.category_id group by cat.category_id, cat.name');
		return $query->result();
	}
}