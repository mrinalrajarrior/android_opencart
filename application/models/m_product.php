<?php


class m_product extends CI_Model
{
    public function create($data){
        $this->db->insert('product',$data);
    }
    public function all($order_column, $order_option){
		if($order_column <= -1) {
			return $this->db->get('product')->result();
		}
        else if($order_option == 0) {
			return $this->db->order_by($order_column, 'ASC')->get('product')->result();
		}
		else {
			return $this->db->order_by($order_column, 'DESC')->get('product')->result();
		}
    }
	public function get($id) {
		return $this->db->where(["product_id"=>$id])->get('product')->row();
	}
	public function update($id,$data) {
		$this->db->where("product_id", $id);
		$this->db->update("product", $data);
	}
	public function delete($id) {
		$this->db->delete('product',['product_id'=>$id]);
	}
	public function product_by_category($category_id,$order_column,$order_option) {
		if($order_column <= -1) {
			$query = $this->db->query('select * from product_category_mapping join product on product.product_id=product_category_mapping.product_id where product_category_mapping.category_id='.$category_id);
		}
		else if($order_option == 0) {
			$query = $this->db->query('select * from product_category_mapping join product on product.product_id=product_category_mapping.product_id where product_category_mapping.category_id='.$category_id.' order by '.$order_column);
		}
		else {
			$query = $this->db->query('select * from product_category_mapping join product on product.product_id=product_category_mapping.product_id where product_category_mapping.category_id='.$category_id.' order by '.$order_column.' desc');
		}
		$results = $query->result_array();
		return($results);
	}
	public function product_by_discount($discount_id,$order_column,$order_option) {
		
		if($order_column <= -1) {
			$query = $this->db->query('select * from product_discount_mapping join product on product.product_id=product_discount_mapping.product_id where discount_id='.$discount_id);
		}
		else if($order_option == 0) {
			$query = $this->db->query('select * from product_discount_mapping join product on product.product_id=product_discount_mapping.product_id where discount_id='.$discount_id. ' order by '.$order_column);
		}
		else {
			$query = $this->db->query('select * from product_discount_mapping join product on product.product_id=product_discount_mapping.product_id where discount_id='.$discount_id.' order by '.$order_column.' desc');
		}
		return($query->result_array());
	}
	public function product_by_discount_category($discount_id,$category_id,$order_column,$order_option) {
		
		if($order_column <= -1) {
			$query = $this->db->query("select * from product_discount_mapping join product on product.product_id=product_discount_mapping.product_id join product_category_mapping on product.product_id=product_category_mapping.product_id where discount_id=".$discount_id." and category_id=".$category_id);
		}
		else if($order_option == 0) {
			$query = $this->db->query("select * from product_discount_mapping join product on product.product_id=product_discount_mapping.product_id join product_category_mapping on product.product_id=product_category_mapping.product_id where discount_id=".$discount_id." and category_id=".$category_id.' order by '.$order_column);
		}
		else {
			$query = $this->db->query("select * from product_discount_mapping join product on product.product_id=product_discount_mapping.product_id join product_category_mapping on product.product_id=product_category_mapping.product_id where discount_id=".$discount_id." and category_id=".$category_id.' order by '.$order_column.' desc');
		}
		
		return($query->result_array());
	}
	public function search($keyword) {
		$query = $this->db->query("select * from product where name like '%".$keyword."%' or company like '%".$keyword."%' or tag like '%".$keyword."%' or description like '%".$keyword."%'");
		return($query->result_array());
	}
}