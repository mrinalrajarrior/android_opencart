<?php


class m_address extends CI_Model
{
    public function create($data){
        $this->db->insert('address',$data);
    }
    public function all(){
        return $this->db->get('address')->result();
    }
	public function get($id) {
		return $this->db->where(["address_id"=>$id])->get('address')->row();
	}
	public function update($id,$data) {
		$this->db->where("address_id", $id);
		$this->db->update("address", $data);
	}
	public function delete($id) {
		$this->db->delete('address',['address_id'=>$id]);
	}
}