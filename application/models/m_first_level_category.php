<?php


class m_first_level_category extends CI_Model
{
    public function create($data){
        $this->db->insert('first_level_category',$data);
    }
    public function all(){
        return $this->db->get('first_level_category')->result();
    }
	public function get($id) {
		return $this->db->where(["category_id"=>$id])->get('first_level_category')->row();
	}
	public function update($id,$data) {
		$this->db->where("category_id", $id);
		$this->db->update("first_level_category", $data);
	}
	public function delete($id) {
		$this->db->delete('first_level_category',['category_id'=>$id]);
	}
	public function get_number_of_items() {
		$query = $this->db->query('select cat.category_id, cat.name, count(*) as number_items from category as cat join product_category_mapping as catmap on cat.category_id=catmap.category_id group by cat.category_id, cat.name');
		return $query->result();
	}
}