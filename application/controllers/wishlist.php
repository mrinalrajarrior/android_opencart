
<?php

class wishlist extends CI_Controller
{
    public function add(){
        if($this->input->method()=="post"){
            $post_data = file_get_contents('php://input');
            $post_data_arr = json_decode($post_data, true);
            
            $this->m_wishlist->create($post_data_arr);

                
            header('Content-Type: application/json');
            echo json_encode(["status"=>"inserted"]);
        }
            
    }

    public function get($customer_id) {
        $this->load->helper('product_helper');
        $wishlists = $this->m_wishlist->get($customer_id);
        $products = [];
        foreach($wishlists as &$wishlist) {
            $attribute = (array)$this->m_attributes->get_by_product_attribute_id($wishlist["product_attribute_id"]);
            $product = get_product_without_attributes($attribute["product_id"]);
            $product["attribute"] = $attribute;
            $wishlist["product"] = $product;
        }
        header('Content-Type: application/json');
        echo json_encode($wishlists);
    }
 
    public function delete($customer_id,$product_id) {
        $this->m_wishlist->delete($customer_id,$product_attribute_id);
        header('Content-type: application/json');
        echo json_encode(["status"=>"deleted"]);
    }

}