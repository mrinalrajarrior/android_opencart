<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function getStateNameByPincode($pincode){
		$ep = "https://www.mapsofindia.com/pincode/$pincode/";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $ep);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$data = curl_exec($ch);
		curl_close($ch);
		$i = strpos($data, "</tr>");
		$data = substr($data, $i+5);
		$i = strpos($data, "</tr>");
		$data = substr($data, $i+5);
		$i = strpos($data, "<td>");
		$data = substr($data, $i+4);
		$j = strpos($data, "</td>");
		$addr = substr($data, 0, $j);  
		header("Conten-type: application/json");
		echo json_encode($addr);
	   }
}
