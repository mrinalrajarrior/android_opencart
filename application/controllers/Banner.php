<?php

class Banner extends CI_Controller
{
    public function add(){
        if($this->input->method()=="post"){
            $post_data_arr = $this->input->post();
           
            if(strlen($post_data_arr["banner_id"])==0){
                $banner_data = array(
                    "name"=>$post_data_arr["name"]
                );
                $this->m_banner->create($banner_data);
                $banner_id = $this->db->insert_id();
                // $banner_discount_mapping_data = array(
                //     "banner_id"=>$banner_id,
                //     "discount_id"=>$post_data_arr["discount_id"]
                // );
                // $this->m_banner_discount_mapping->create($banner_discount_mapping_data);
                $image_id = $this->do_upload('image');
                if($image_id != "") {
                    //$this->m_image->create(["image_id"=>$image_id,"banner_id"=>$banner_id,"url"=>"http://localhost/android/index.php/product/product_by_discount/".$post_data_arr["discount_id"]]);
                    $this->m_banner_discount_mapping->create(["image_id"=>$image_id,"discount_id"=>$post_data_arr["discount_id"],"banner_id"=>$banner_id,"url"=>"product/product_by_discount/".$post_data_arr["discount_id"]]);
                }
                //create
            }
            else{
                $banner_id = $post_data_arr["banner_id"];
                //update
                // $banner_discount_mapping_data = array(
                //     "banner_id"=>$post_data_arr["banner_id"],
                //     "discount_id"=>$post_data_arr["discount_id"]
                // );
                // $this->m_banner_discount_mapping->create($banner_discount_mapping_data);
                $image_id = $this->do_upload('image');
                if($image_id != "") {
                    //$this->m_image->create(["image_id"=>$image_id,"banner_id"=>$post_data_arr["banner_id"],"url"=>"http://localhost/android/index.php/product/product_by_discount/".$post_data_arr["discount_id"]]);
                    $this->m_banner_discount_mapping->create(["image_id"=>$image_id,"discount_id"=>$post_data_arr["discount_id"],"banner_id"=>$banner_id,"url"=>"product/product_by_discount/".$post_data_arr["discount_id"]]);
                }
            }

			$menu = $this->load->view('menu', null, true);
            $header = $this->load->view('header', null, true);
            $data = array(
                "menu"=>$menu,
                "header"=>$header
            );
            $this->load->view('banner/banner_add',$data);
        }
    }

    public function all(){
        $banners=$this->m_banner->all();
        foreach($banners as &$banner) {
            //$banner_images = $this->m_image->get_by_banner_id($banner["banner_id"]);
            $banner_discounts = $this->m_banner_discount_mapping->get_by_banner_id($banner["banner_id"]);
            //$discounts= [];
            // foreach($banner_discounts as $banner_discount){
            //     $discount = $this->m_discount->get($banner_discount["discount_id"]);
            //     array_push($discounts,$discount);
            // }
            //$banner["images"] = $banner_images;
            $banner["offers"] = $banner_discounts;
        }
		header('Content-Type: application/json');
        echo(json_encode($banners));
    }

    public function get($id){
        $banner = (array)$this->m_banner->get($id);
        $banner['offers'] = $this->m_banner_discount_mapping->get_by_banner_id($banner["banner_id"]);
		header('Content-Type: application/json');
        echo json_encode($banner);
    }

    public function update_api(){
		if($this->input->method()=="post") {
            $post_data = file_get_contents('php://input');
            $post_data_arr = json_decode($post_data, true);
            // var_dump($post_data_arr); 
            // exit();
            $this->m_banner->update($post_data_arr["banner_id"], ["name"=>$post_data_arr["name"]]);
            $this->m_banner_discount_mapping->delete_by_banner_id($post_data_arr["banner_id"]);
            foreach($post_data_arr["offers"] as $offer) {
                $this->m_banner_discount_mapping->create($offer);
            }
            header('Content-Type: application/json');
            echo json_encode(["status"=>"successful"]);
        }
		// $menu = $this->load->view('menu', null, true);
        // $header = $this->load->view('header', null, true);
        // $data = array(
        //     "menu"=>$menu,
        //     "header"=>$header,
        //     "banner_id"=>$post_data_arr["banner_id"]
        // );
        // $this->load->view('banner/banner_update',$data);
    }

	public function delete($id) {
        // var_dump($id);
        // exit();
        $this->m_banner->delete($id);
        $this->m_banner_discount_mapping->delete_by_banner_id($id);
        $menu = $this->load->view('menu', null, true);
        $header = $this->load->view('header', null, true);
        $data = array(
            "menu"=>$menu,
            "header"=>$header
        );
        $this->load->view('banner/banner_view',$data);
    }
    public function get_number_of_items() {
        header('Content-type: Application/json');
        echo json_encode($this->m_category->get_number_of_items());
    }
    private function do_upload($name)
    {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['encrypt_name']        = true;
        /*$config['max_size']             = 100;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;*/

        $this->load->library('upload', $config);

        if ($this->upload->do_upload($name))
        {
            $data = $this->upload->data();
            return $data['file_name'];
        }
        else {
            return false;
        }
    }
}