<?php


class product extends CI_Controller
{
    public function add(){
        if($this->input->method()=="post"){
            $post_data_arr = $this->input->post();
            // var_dump($post_data_arr);
            // exit();
            if(isset($post_data_arr["product"]) && sizeof($post_data_arr["product"])>0) {
                //$post_data_arr["product"]["image_id"] = $this->do_upload();
                $images = $this->upload_files($_FILES['images']);
                
                $this->m_product->create($post_data_arr['product']);
                $insert_id =  $this->db->insert_id();
                if($images != false) {
                    foreach($images as $image){
                        $data = array(
                            "image_id"=>$image,
                            "product_id"=>$insert_id                        
                        );
                        $this->m_image->create($data);
                    }
                }
                
            }
            
            if(isset($post_data_arr["attributes"]) && sizeof($post_data_arr["attributes"])>0) {
                foreach($post_data_arr["attributes"] as $attribute) {
                    $attribute["product_id"] = $insert_id;
                    $this->m_attributes->create($attribute);
                }
                
            }
            if(isset($post_data_arr["category"]) && sizeof($post_data_arr["category"])>0) {
                foreach($post_data_arr["category"] as $cat) {
                    $product_category_arr["category_id"] = $cat;
                    $product_category_arr["product_id"] = $insert_id;
                    $this->m_product_category_mapping->create($product_category_arr);
                }
                
            }

            if(isset($post_data_arr["discount"]) && sizeof($post_data_arr["discount"])>0) {
                $product_discount_arr = $post_data_arr["discount"];
                $product_discount_arr["product_id"] = $insert_id;
                $this->m_product_discount_mapping->create($product_discount_arr);
            }
            $menu = $this->load->view('menu', null, true);
		    $header = $this->load->view('header', null, true);
            $data = array(
                "menu"=>$menu,
                "header"=>$header
            );
            $this->load->view('product/product_add',$data);
        }
    }

    public function all($order_column = -1, $order_option = 0){
        $prods=$this->m_product->all($order_column, $order_option);
        $response = array();
        foreach($prods as $prod){
            $prod = (array)$prod;
            $attr = (array)$this->m_attributes->get($prod["product_id"]);
            $prod_categories = $this->m_product_category_mapping->get($prod["product_id"]);
            $category = [];
            $images = [];
            if(isset($prod_categories) && sizeof($prod_categories)>0) {
                foreach($prod_categories as $prod_category) {
                    $cat = (array)$this->m_category->get($prod_category["category_id"]);    
                    array_push($category,$cat);
                }   
            }
            $prod_discount = (array)$this->m_product_discount_mapping->get($prod["product_id"]);
            $discount = [];
            if(isset($prod_discount) && sizeof($prod_discount)>0) {
                $discount = (array)$this->m_discount->get($prod_discount["discount_id"]);
            }
            if(isset($category["second_level_category_id"])) {
                $second_level_category = (array)$this->m_second_level_category->get($category["second_level_category_id"]);
            }
            $images = $this->m_image->get_by_product_id($prod["product_id"]);
            $res = array(
                "product" => $prod,
                "images" => $images,
                "attributes" => $attr,
                "category" => $category,
                "discount"=>$discount
            );
            array_push($response, $res);
        }

		header('Content-Type: application/json');
        echo(json_encode($response));
    }

    public function get($id){
        $prod=(array)$this->m_product->get($id);
        $attr = (array)$this->m_attributes->get($prod["product_id"]);
        $prod_categories = (array)$this->m_product_category_mapping->get($prod["product_id"]);
        $category = [];
        foreach($prod_categories as $prod_category) {
            $cat = $this->m_category->get($prod_category["category_id"]);
            array_push($category, $cat);
        }
        $images = $this->m_image->get_by_product_id($prod["product_id"]);
        $prod_discount = (array)$this->m_product_discount_mapping->get($prod["product_id"]);
        $discount = (array)$this->m_discount->get($prod_discount["discount_id"]);
        $res = array(
            "product" => $prod,
            "images" => $images,
            "attributes" => $attr,
            "category" => $category,
            "discount" => $discount
        );
		header('Content-Type: application/json');
        echo json_encode($res);
    }

    public function get_multiple(){
        $post_data = file_get_contents('php://input');
        $post_data_arr = json_decode($post_data, true);
        $ids = $post_data_arr['productIds'];
        $products = [];
        foreach ($ids as $id) {
            $prod=(array)$this->m_product->get($id);
            $attr = (array)$this->m_attributes->get($prod["product_id"]);
            $prod_categories = (array)$this->m_product_category_mapping->get($prod["product_id"]);
            $category = [];
            foreach($prod_categories as $prod_category) {
                $cat = $this->m_category->get($prod_category["category_id"]);
                array_push($category, $cat);
            }
            $images = $this->m_image->get_by_product_id($prod["product_id"]);
            $prod_discount = (array)$this->m_product_discount_mapping->get($prod["product_id"]);
            $discount = (array)$this->m_discount->get($prod_discount["discount_id"]);
            $products[] = array(
                "product" => $prod,
                "images" => $images,
                "attributes" => $attr,
                "category" => $category,
                "discount" => $discount
            );
        }
        $res= $products;

        header('Content-Type: application/json');
        echo json_encode($res);
    }


    public function update($product_id) {
		if($this->input->method()=="post") {
            $post_data_arr = $this->input->post();
            // var_dump($post_data_arr);
            // exit();
            if(isset($post_data_arr["product"]) && sizeof($post_data_arr["product"])>0) {
                $images = $this->upload_files($_FILES['images']);
                if($images != false) {
                    $this->m_image->delete_by_product_id($product_id);
                    foreach($images as $image){
                        $data = array(
                            "image_id"=>$image,
                            "product_id"=>$product_id                        
                        );
                        $this->m_image->create($data);
                    }
                }
                
                
                //$image_id = $this->do_upload();
                // if(strlen($image_id)>0) {
                //     $post_data_arr["product"]["image_id"] = $this->do_upload();
                // }   
                $this->m_product->update($product_id, $post_data_arr["product"]);
            }
            if(isset($post_data_arr["attributes"]) && sizeof($post_data_arr["attributes"])>0) {
                $attrs = $this->m_attributes->get($product_id);
                $prod_attr_id_found = [];
                foreach($post_data_arr["attributes"] as &$attribute) {
                    if($attribute["product_attribute_id"] != "" && $attribute["product_id"] != "" ) {
                        array_push($prod_attr_id_found,$attribute["product_attribute_id"]);
                        $this->m_attributes->update_by_product_attribute_id($attribute["product_attribute_id"], $attribute);
                        unset($attribute);
                    }
                    else {
                        $attribute["product_id"] = (int)$product_id;
                        unset($attribute["product_attribute_id"]);
                        $this->m_attributes->create($attribute);
                    }
                }
                foreach($attrs as $attr) {
                    $found = false;
                    foreach($prod_attr_id_found as $prod_attr_id) {
                        if($attr["product_attribute_id"] == $prod_attr_id) {
                            $found = true;
                        }
                    }
                    if(!$found) {
                        $this->m_attributes->delete_by_product_attribute_id($attr["product_attribute_id"]);
                    }
                }   
            }
            if(isset($post_data_arr["category"]) && sizeof($post_data_arr["category"])>0) {
                $this->m_product_category_mapping->delete($product_id);
                foreach($post_data_arr["category"] as $category_id) {
                    $this->m_product_category_mapping->create(["product_id"=>$product_id,"category_id"=>$category_id]);
                }
                // if(isset($product_categories)){
                //     $this->m_product_category_mapping_delete()
                //     $this->m_product_category_mapping->update($product_id, $post_data_arr["category"]);
                // }
                // else {
                //     $product_category_create = $post_data_arr["category"];
                //     $product_category_create["product_id"]=$product_id;
                //     $this->m_product_category_mapping->create($product_category_create);
                // }
            }
            if(isset($post_data_arr["discount"]) && sizeof($post_data_arr["discount"])>0) {
                $product_discount = $this->m_product_discount_mapping->get($product_id);
                if(isset($product_discount)){
                    $this->m_product_discount_mapping->update($product_id, $post_data_arr["discount"]);
                }
                else{
                    $product_discount_create = $post_data_arr["discount"];
                    $product_discount_create["product_id"]=$product_id;
                    $this->m_product_discount_mapping->create($product_discount_create);
                }
                
            }
            
        }
		$menu = $this->load->view('menu', null, true);
        $header = $this->load->view('header', null, true);
        $data = array(
            "menu"=>$menu,
            "header"=>$header,
            "product_id_update"=>$product_id
        );
        $this->load->view('product/product_update',$data);
    }

	public function delete($id) {
        $this->m_product->delete($id);
        $this->m_attributes->delete($id);
        $this->m_product_category_mapping->delete($id);
        $this->m_product_discount_mapping->delete($id);
        $menu = $this->load->view('menu', null, true);
        $header = $this->load->view('header', null, true);
        $data = array(
            "menu"=>$menu,
            "header"=>$header
        );
        $this->load->view('product/product_delete',$data);
    }

    public function product_by_category($category_id,$order_column = -1, $order_option = 0) {
        $prods = $this->m_product->product_by_category($category_id,$order_column,$order_option);
        $response = array();
        foreach($prods as $prod){
            $prod = (array)$prod;
            $attr = (array)$this->m_attributes->get($prod["product_id"]);
            $prod_categories = (array)$this->m_product_category_mapping->get($prod["product_id"]);
            $category = [];
            if(isset($prod_categories) and sizeof($prod_categories)>0) {
                foreach($prod_categories as $prod_category){
                    $cat = (array)$this->m_category->get($prod_category["category_id"]);
                    array_push($category,$cat);
                }  
            }
            $prod_discount = (array)$this->m_product_discount_mapping->get($prod["product_id"]);
            $discount = [];
            if(isset($prod_discount) && sizeof($prod_discount)>0) {
                $discount = (array)$this->m_discount->get($prod_discount["discount_id"]);
            }
            $images = $this->db->select("image_id")->get_where("image", ["product_id" => $prod["product_id"]])->result();
            $res = array(
                "product" => $prod,
                "attributes" => $attr,
                "category" => $category,
                "discount"=>$discount,
                "images" => $images
            );
            array_push($response, $res);
        }
        header('Content-type: application/json');
        echo(json_encode($response));
    }
    
    public function product_by_discount($discount_id,$order_column = -1, $order_option = 0) {
        $prods = $this->m_product->product_by_discount($discount_id,$order_column,$order_option);
        $response = array();
        foreach($prods as $prod){
            $prod = (array)$prod;
            $attr = (array)$this->m_attributes->get($prod["product_id"]);
            $prod_categories = (array)$this->m_product_category_mapping->get($prod["product_id"]);
            $category = [];
            if(isset($prod_categories) and sizeof($prod_categories)>0) {
                foreach($prod_categories as $prod_category){
                    $cat = (array)$this->m_category->get($prod_category["category_id"]);
                    array_push($category,$cat);
                }  
            }
            $prod_discount = (array)$this->m_product_discount_mapping->get($prod["product_id"]);
            $discount = [];
            if(isset($prod_discount) && sizeof($prod_discount)>0) {
                $discount = (array)$this->m_discount->get($prod_discount["discount_id"]);
            }
            
            $res = array(
                "product" => $prod,
                "attributes" => $attr,
                "category" => $category,
                "discount"=>$discount
            );
            array_push($response, $res);
        }

        header('Content-type: application/json');
        echo(json_encode($response));
    }

    public function product_by_discount_category($discount_id,$category_id,$order_column = -1, $order_option = 0) {
        $prods = $this->m_product->product_by_discount_category($discount_id,$category_id,$order_column,$order_option);
        $response = array();
        foreach($prods as $prod){
            $prod = (array)$prod;
            $attr = (array)$this->m_attributes->get($prod["product_id"]);
            $prod_categories = (array)$this->m_product_category_mapping->get($prod["product_id"]);
            $category = [];
            if(isset($prod_categories) and sizeof($prod_categories)>0) {
                foreach($prod_categories as $prod_category){
                    $cat = (array)$this->m_category->get($prod_category["category_id"]);
                    array_push($category,$cat);
                }  
            }
            $prod_discount = (array)$this->m_product_discount_mapping->get($prod["product_id"]);
            $discount = [];
            if(isset($prod_discount) && sizeof($prod_discount)>0) {
                $discount = (array)$this->m_discount->get($prod_discount["discount_id"]);
            }
            
            $res = array(
                "product" => $prod,
                "attributes" => $attr,
                "category" => $category,
                "discount"=>$discount
            );
            array_push($response, $res);
        }

        header('Content-type: application/json');
        echo(json_encode($response));
    }

    public function search_product() {
        if($this->input->method()=="post") {
            $this->load->helper("product_helper");
            $post_data = file_get_contents("php://input");
            $post_data_arr = json_decode($post_data, true);
            $prods = $this->m_product->search($post_data_arr["keyword"]);
            $products = [];
            foreach($prods as $prod) {
                $product = get_product($prod["product_id"]);
                array_push($products,$product);
            }
            header('Content-type: application/json');
            echo(json_encode($products));
        }
        
    }

    private function do_upload()
    {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['encrypt_name']        = true;
        /*$config['max_size']             = 100;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;*/

        $this->load->library('upload', $config);

        if ($this->upload->do_upload("image"))
        {
            $data = $this->upload->data();
            return $data['file_name'];
        }
        else {
            return '';
        }
    }
    private function upload_files($files)
    {
        $config = array(
            'upload_path'   => './uploads/',
            'allowed_types' => 'jpg|gif|png',
            'overwrite'     => 1,           
            'encrypt_name' => true            
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
               $data = $this->upload->data();
               $images[] = $data['file_name'];
            } else {
                return false;
            }
        }

        return $images;
    }
}