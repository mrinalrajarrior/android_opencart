<?php


class Product_attributes extends CI_Controller
{
    public function all($order_column = -1, $order_option = 0){
        $prods=$this->m_product->all($order_column, $order_option);
        $response = array();
        foreach($prods as $prod){
            $prod = (array)$prod;
            $attr = (array)$this->m_attributes->get($prod["product_id"]);
            $prod_category = (array)$this->m_product_category_mapping->get($prod["product_id"]);
            $category = [];
            if(isset($prod_category) and sizeof($prod_category)>0) {
                $category = (array)$this->m_category->get($prod_category["category_id"]);
            }
            $prod_discount = (array)$this->m_product_discount_mapping->get($prod["product_id"]);
            $discount = [];
            if(isset($prod_discount) && sizeof($prod_discount)>0) {
                $discount = (array)$this->m_discount->get($prod_discount["discount_id"]);
            }
            if(isset($category["second_level_category_id"])) {
                $second_level_category = (array)$this->m_second_level_category->get($category["second_level_category_id"]);
            }
            
            $res = array(
                "product" => $prod,
                "attributes" => $attr,
                "category" => $category,
                "discount"=>$discount,
                "second_level_category"=>$second_level_category
            );
            array_push($response, $res);
        }

		header('Content-Type: application/json');
        echo(json_encode($response));
    }

    public function get($id){
        $prod=(array)$this->m_product->get($id);
        $attr = (array)$this->m_attributes->get($prod["product_id"]);
        $prod_category = (array)$this->m_product_category_mapping->get($prod["product_id"]);
        $category = (array)$this->m_category->get($prod_category["category_id"]);
        $prod_discount = (array)$this->m_product_discount_mapping->get($prod["product_id"]);
        $discount = (array)$this->m_discount->get($prod_discount["discount_id"]);
        $res = array(
            "product" => $prod,
            "attributes" => $attr,
            "category" => $category,
            "discount" => $discount
        );
		header('Content-Type: application/json');
        echo json_encode($res);
    }


    public function update($id){
		if($this->input->method()=="post") {
            $post_data_arr = $this->input->post();
            if(isset($post_data_arr["product"]) && sizeof($post_data_arr["product"])>0) {
                $image_id = $this->do_upload();
                if(strlen($image_id)>0) {
                    $post_data_arr["product"]["image_id"] = $this->do_upload();
                }
                $this->m_product->update($id, $post_data_arr["product"]);
            }
            if(isset($post_data_arr["attributes"]) && sizeof($post_data_arr["attributes"])>0) {
                $this->m_attributes->update($id, $post_data_arr["attributes"]);
            }
            if(isset($post_data_arr["category"]) && sizeof($post_data_arr["category"])>0) {
                $product_category = $this->m_product_category_mapping->get($id);
                if(isset($product_category)){
                    $this->m_product_category_mapping->update($id, $post_data_arr["category"]);
                }
                else {
                    $product_category_create = $post_data_arr["category"];
                    $product_category_create["product_id"]=$id;
                    $this->m_product_category_mapping->create($product_category_create);
                }
            }
            if(isset($post_data_arr["discount"]) && sizeof($post_data_arr["discount"])>0) {
                $product_discount = $this->m_product_discount_mapping->get($id);
                if(isset($product_discount)){
                    $this->m_product_discount_mapping->update($id, $post_data_arr["discount"]);
                }
                else{
                    $product_discount_create = $post_data_arr["discount"];
                    $product_discount_create["product_id"]=$id;
                    $this->m_product_discount_mapping->create($product_discount_create);
                }
                
            }
            
        }
		$menu = $this->load->view('menu', null, true);
        $header = $this->load->view('header', null, true);
        $data = array(
            "menu"=>$menu,
            "header"=>$header,
            "product_id_update"=>$id
        );
        $this->load->view('product/product_update',$data);
    }

	public function delete($id) {
        $this->m_product->delete($id);
        $this->m_attributes->delete($id);
        $this->m_product_category_mapping->delete($id);
        $this->m_product_discount_mapping->delete($id);
        $menu = $this->load->view('menu', null, true);
        $header = $this->load->view('header', null, true);
        $data = array(
            "menu"=>$menu,
            "header"=>$header
        );
        $this->load->view('product/product_delete',$data);
    }

    public function product_by_category($category_id,$order_column = -1, $order_option = 0) {
        $prods = $this->m_product->product_by_category($category_id,$order_column,$order_option);
        $response = array();
        foreach($prods as $prod){
            $prod = (array)$prod;
            $attr = (array)$this->m_attributes->get($prod["product_id"]);
            $prod_category = (array)$this->m_product_category_mapping->get($prod["product_id"]);
            $category = [];
            if(isset($prod_category) and sizeof($prod_category)>0) {
                $category = (array)$this->m_category->get($prod_category["category_id"]);
            }
            $prod_discount = (array)$this->m_product_discount_mapping->get($prod["product_id"]);
            $discount = [];
            if(isset($prod_discount) && sizeof($prod_discount)>0) {
                $discount = (array)$this->m_discount->get($prod_discount["discount_id"]);
            }
            
            $res = array(
                "product" => $prod,
                "attributes" => $attr,
                "category" => $category,
                "discount"=>$discount
            );
            array_push($response, $res);
        }
        header('Content-type: application/json');
        echo(json_encode($response));
    }
    
    public function product_by_discount($discount_id,$order_column = -1, $order_option = 0) {
        $prods = $this->m_product->product_by_discount($discount_id,$order_column,$order_option);
        $response = array();
        foreach($prods as $prod){
            $prod = (array)$prod;
            $attr = (array)$this->m_attributes->get($prod["product_id"]);
            $prod_category = (array)$this->m_product_category_mapping->get($prod["product_id"]);
            $category = [];
            if(isset($prod_category) and sizeof($prod_category)>0) {
                $category = (array)$this->m_category->get($prod_category["category_id"]);
            }
            $prod_discount = (array)$this->m_product_discount_mapping->get($prod["product_id"]);
            $discount = [];
            if(isset($prod_discount) && sizeof($prod_discount)>0) {
                $discount = (array)$this->m_discount->get($prod_discount["discount_id"]);
            }
            
            $res = array(
                "product" => $prod,
                "attributes" => $attr,
                "category" => $category,
                "discount"=>$discount
            );
            array_push($response, $res);
        }

        header('Content-type: application/json');
        echo(json_encode($response));
    }

    public function product_by_discount_category($discount_id,$category_id,$order_column = -1, $order_option = 0) {
        $prods = $this->m_product->product_by_discount_category($discount_id,$category_id,$order_column,$order_option);
        $response = array();
        foreach($prods as $prod){
            $prod = (array)$prod;
            $attr = (array)$this->m_attributes->get($prod["product_id"]);
            $prod_category = (array)$this->m_product_category_mapping->get($prod["product_id"]);
            $category = [];
            if(isset($prod_category) and sizeof($prod_category)>0) {
                $category = (array)$this->m_category->get($prod_category["category_id"]);
            }
            $prod_discount = (array)$this->m_product_discount_mapping->get($prod["product_id"]);
            $discount = [];
            if(isset($prod_discount) && sizeof($prod_discount)>0) {
                $discount = (array)$this->m_discount->get($prod_discount["discount_id"]);
            }
            
            $res = array(
                "product" => $prod,
                "attributes" => $attr,
                "category" => $category,
                "discount"=>$discount
            );
            array_push($response, $res);
        }

        header('Content-type: application/json');
        echo(json_encode($response));
    }

    private function do_upload()
    {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['encrypt_name']        = true;
        /*$config['max_size']             = 100;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;*/

        $this->load->library('upload', $config);

        if ($this->upload->do_upload("image"))
        {
            $data = $this->upload->data();
            return $data['file_name'];
        }
        else {
            return '';
        }
    }
}