<?php

class orders extends CI_Controller
{
    public function add($customer_id, $address_id, $txnId, $mode, $shipping, $total){
        if($this->input->method()=="post"){
            $post_data = file_get_contents('php://input');
            $post_data_arr = json_decode($post_data, true);
            $cart = $this->db->where('customer_id', $customer_id)->get('cart')->row();
            $cart_items = $this->db->where('cart_id', $cart->cart_id)->get('cart_product_mapping')->result();
            $address = $this->db->where('address_id', $address_id)->get('address')->row();
            $products = [];
            $tot_price = 0.0;
            foreach($cart_items as $product) {
                $product_attribute_id = $product->product_attribute_id;
                $product_attribute = $this->db->where('product_attribute_id', $product_attribute_id)->get('attributes')->row();
                $quantity = $product->quantity;
                $data = array (
                    "product_attribute_id"=>$product_attribute_id,
                    "quantity"=>$quantity
                );
                $tot_price += $product_attribute->price*$quantity;
                array_push($products, $data);
            }

            /*
            Pricing calculation
            */
            $price = [];
            $price["total"] = 0;
            $price["tax_total"] = 0;

            foreach ($products as $product) {
                $product_attribute = $this->db->where('product_attribute_id', $product['product_attribute_id'])->get('attributes')->row();
                $final_price = $product_attribute->final_price;
                $tax_percentage = $product_attribute->tax;
                $tax_amt = $final_price * 100 / (100 + 100*$tax_percentage);
                $price["total"] += $final_price;
                $price["tax_total"] += $final_price - $tax_amt;
            }

            $total_wo_shipping = $price["total"] - $price["tax_total"];
            $shipping = $this->config->item('shipping');
            $free_shipping = $this->config->item('free_shipping');
            $has_free_shipping = $this->config->item('has_free_shipping');
            $tax_total = $price["tax_total"];

            if($has_free_shipping && $total_wo_shipping >= $free_shipping){
                $shipping = 0;
            }
            /*
            Ends
            */

            $order_data = array(
                "amount" => $tot_price,
                "address_id" => $address_id,
                "customer_id" => $customer_id,
                "address_line_1" => $address->address_line_1,
                "address_line_2" => $address->address_line_2,
                "zipcode" => $address->zipcode,
                "city" => $address->city,
                "state" => $address->state,
                "country" => $address->country,
                "shipping" => $shipping,
                "customer_name" => $address->customer_name,
                "phone_number" => $address->mobile,
                "txn_id" => $txnId,
                "mode" => $mode,
                "tax_amount" => sprintf("%.0f", $tax_total),
                "total_amt" => sprintf("%.0f", $total_wo_shipping + $shipping),
                "before_tax" => sprintf("%.0f", $total_wo_shipping - $tax_total),
                "before_tax_with_shipping" => sprintf("%.0f", $total_wo_shipping - $tax_total + $shipping),
                "shipping" => sprintf("%.0f", $shipping),
            );
            $this->m_orders->create($order_data);
            $insert_id =  $this->db->insert_id();
            foreach($products as $product) {
                $product["order_id"]=$insert_id;
                $this->m_product_order_mapping->create($product);
            }
            //Delete product in cart product mapping
            foreach($cart_items as $cart_item) {
                $this->m_cart_product_mapping->delete($cart_item->cart_id,$cart_item->product_attribute_id);
            }


           



			header('Content-Type: application/json');
            echo json_encode(array(
                "order_id" => $insert_id
            ));
        }
    }

    public function all(){
        $this->load->helper('product_helper');
        $orders=$this->m_orders->all();
        $response = [];
        $products = [];
        foreach($orders as $order){
            //$order['status'] = $this->db->where('status_id', $order['status_id'])->get('order_status')->row();
            $order = (array)$order;
            $product_orders = $this->m_product_order_mapping->get($order["order_id"]);
            foreach($product_orders as $product_order) {
                $attribute = (array)$this->m_attributes->get_by_product_attribute_id($product_order->product_attribute_id);
                if($attribute != null) {
                    $product = get_product_without_attributes($attribute["product_id"]);
                    $product["attributes"] = $attribute;
                    $product["attributes"]["quantity_requested"] = $product_order->quantity;
                    array_push($products, $product); 
                }
                
            }
            $customer = (array)get_customer($order["customer_id"]);
            $order["status"] = $this->m_order_status->get($order["status_id"])->status;
            $res = array(
                "orders"=>$order,
                "products"=>$products,
                "customer"=>$customer,
            );
            array_push($response, $res);
        }
		header('Content-Type: application/json');
        echo(json_encode($response));
    }

    public function get($id){
        $this->load->helper('product_helper');
        $order=(array)$this->m_orders->get($id);
        //$order['status'] = $this->db->where('status_id', $order['status_id'])->get('order_status')->row();
        if($order != null) {
            $products = [];
            $product_orders = $this->m_product_order_mapping->get($order["order_id"]);
            foreach($product_orders as $product_order) {
                $attribute = (array)$this->m_attributes->get_by_product_attribute_id($product_order->product_attribute_id);
                if($attribute != null) {
                    $product = get_product_without_attributes($attribute["product_id"]);
                    $product["attributes"] = $attribute;
                    $product["attributes"]["quantity_requested"] = $product_order->quantity;
                    array_push($products, $product);
                }
                
            }
            $order["status"] = $this->m_order_status->get($order["status_id"])->status;
            $res = array(
                "orders"=>$order,
                "products"=>$products
            );
        }
        else {
            $res = ["status"=>"No order found"];
        }
        

		header('Content-Type: application/json');
        echo json_encode($res);
    }

    public function get_by_customer($id){
        $this->load->helper('product_helper');
        $response = [];
        $orders=(array)$this->m_orders->get_by_customer($id);
        if($orders != null) {
            foreach($orders as $order) {
                $products = [];
                $product_orders = $this->m_product_order_mapping->get($order["order_id"]);
                foreach($product_orders as $product_order) {
                    $attribute = $this->m_attributes->get_by_product_attribute_id($product_order->product_attribute_id);
                    $product = get_product_without_attributes($attribute->product_id);
                    $product["attributes"] = $attribute;
                    $product["attributes"]->quantity_requested = $product_order->quantity;
                    array_push($products, $product);
                }
                //$customer = (array)get_customer($order["customer_id"]);
                $order["status"] = $this->m_order_status->get($order["status_id"])->status;
                $res = array(
                    "orders"=>$order,
                    "products"=>$products,
                    //"customer"=>$customer,
                );
                array_push($response,$res);
            }
        }
        else {
            $response = ["status"=>"No order found"];
        }
        

		header('Content-Type: application/json');
        echo json_encode($response);
    }
    
    public function update($id){
		if($this->input->method()=="post") {
            $post_data = file_get_contents('php://input');
            $post_data_arr = json_decode($post_data, true);
            date_default_timezone_set('Asia/Kolkata');
            $post_data_arr["date_modified"] = date('Y-m-d H:i:s');
           
            $this->m_orders->update($id, $post_data_arr);

            header('Content-Type: application/json');
            echo(json_encode(["affected_rows"=>$this->db->affected_rows()]));
        }
		else {
            header('Content-Type: application/json');
            echo(json_encode("not a post request"));
        }
    }

	public function delete($id) {
        $this->m_orders->delete($id);
        echo(json_encode(["status"=>"deleted"]));
    }

    public function cancel($id) {
        $this->m_orders->cancel($id);
        echo(json_encode(["status"=>"cancelled"]));
    }
    
    public function test_api() {
        $post_data = file_get_contents('php://input');
        $post_data_arr = json_decode($post_data, true);
        header('Content-Type: application/json');
        echo(json_encode(["status"=>"success"]));
	}
}