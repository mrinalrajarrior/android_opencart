<?php

class cart extends CI_Controller
{
    public function add(){
        if($this->input->method()=="post"){
            $post_data = file_get_contents('php://input');
            $post_data_arr = json_decode($post_data, true);
            $products = [];
            foreach($post_data_arr["product"] as $product) {
                $product_attribute_id = $product["product_attribute_id"];
                $quantity = $product["quantity"];
                $data = array (
                    "product_attribute_id"=>$product_attribute_id,
                    "quantity"=>$quantity
                );
                array_push($products, $data);
            }
            unset($post_data_arr["product"]);
            $cart = $this->m_cart->get($post_data_arr["customer_id"]);
            if($cart == null) {
                $this->m_cart->create($post_data_arr);
                $cart_id = $this->db->insert_id();
            }
            else {
                $cart_id = $cart->cart_id;
            }
            
            foreach($products as $product) {
                $product["cart_id"]=$cart_id;
                $this->m_cart_product_mapping->create($product);
            }
            header('Content-Type: application/json');
            echo json_encode(["cart_id"=>$cart_id]);
        }
    }

    public function all(){
        $this->load->helper('product_helper');
        $carts=$this->m_cart->all();
        $response = [];
        $products = [];
        foreach($carts as $cart){
            $cart = (array)$cart;
            $cart_products = $this->m_cart_product_mapping->get($cart["cart_id"]);
            foreach($cart_products as $cart_product) {
                $attribute = $this->m_attributes->get_by_product_attribute_id($cart_product->product_attribute_id);
                $product = get_product_without_attributes($attribute->product_id);
                if($product != null) {
                    $product["product"]["quantity_requested"] = $cart_product->quantity;
                    $product["attributes"] = $attribute;
                    array_push($products, $product);
                }
            }
            $customer = (array)get_customer($cart["customer_id"]);
            $res = array(
                "cart"=>$cart,
                "products"=>$products,
                "customer"=>$customer,
            );
            array_push($response, $res);
        }
        header('Content-Type: application/json');
        echo(json_encode($response));
    }

    public function get($customer_id){
        $this->load->helper('product_helper');
        $cart=(array)$this->m_cart->get($customer_id);
        $products = [];
        if($cart != null) {
            $cart_products = $this->m_cart_product_mapping->get($cart["cart_id"]);
            foreach($cart_products as $cart_product) {
                $attribute = $this->m_attributes->get_by_product_attribute_id($cart_product->product_attribute_id);
                $product = get_product_without_attributes($attribute->product_id);
                if($product != null) {
                    $product["product"]["quantity_requested"] = $cart_product->quantity;
                    $product["attributes"] = $attribute;
                    array_push($products, $product);
                }
            }
            $customer = (array)get_customer($cart["customer_id"]);

            $price = [];
            $price["total"] = 0;
            $price["tax_total"] = 0;

            foreach ($products as $product) {
                $final_price = $product["attributes"]->final_price;
                $tax_percentage = $product["attributes"]->tax;
                $tax_amt = $final_price * 100 / (100 + 100*$tax_percentage);
                $price["total"] += $final_price;
                $price["tax_total"] += $final_price - $tax_amt;
            }

            $total_wo_shipping = $price["total"] - $price["tax_total"];
            $shipping = $this->config->item('shipping');
            $free_shipping = $this->config->item('free_shipping');
            $has_free_shipping = $this->config->item('has_free_shipping');
            $tax_total = $price["tax_total"];

            if($has_free_shipping && $total_wo_shipping >= $free_shipping){
                $shipping = 0;
            }

            $res = array(
                "cart"=>$cart,
                "products"=>$products,
                "customer"=>$customer,
                "tax_amount" => sprintf("%.0f", $tax_total),
                "total" => sprintf("%.0f", $total_wo_shipping + $shipping),
                "before_tax" => sprintf("%.0f", $total_wo_shipping - $tax_total),
                "before_tax_with_shipping" => sprintf("%.0f", $total_wo_shipping - $tax_total + $shipping),
                "shipping" => sprintf("%.0f", $shipping),
                "free_shipping" => sprintf("%.0f", $free_shipping),
                "has_free_shipping" => $has_free_shipping,
            );
            header('Content-Type: application/json');
            echo json_encode($res);
        }
        else {
            header('Content-Type: application/json');
            echo json_encode(["cart"=>[]]);
        }
        
    }

    public function update($customer_id){
        if($this->input->method()=="post") {
            $post_data = file_get_contents('php://input');
            $post_data_arr = json_decode($post_data, true);
           
            $cart = $this->m_cart->get($customer_id);

            $final_products = [];
            $products = $post_data_arr["product"];
            foreach($products as $product) {
                $product["cart_id"] = $cart->cart_id;
                $this->m_cart_product_mapping->update($product["cart_id"],$product["product_attribute_id"],$product);
            }
            header('Content-Type: application/json');
            echo(json_encode(["affected_rows"=>$this->db->affected_rows()]));
        }
        else {
            header('Content-Type: application/json');
            echo(json_encode("not a post request"));
        }
    }

    public function delete($customer_id,$product_attribute_id) {

        $cart = $this->m_cart->get($customer_id);
        $remaining_row = $this->m_cart_product_mapping->delete($cart->cart_id,$product_attribute_id);
        if($remaining_row ==  null) {
            $this->m_cart->delete($customer_id);
        }
        header("Content-type: application/json");
        echo(json_encode(["status"=>"deleted"]));
    }
    
    public function test_api() {
        $post_data = file_get_contents('php://input');
        $post_data_arr = json_decode($post_data, true);
        header('Content-Type: application/json');
        echo(json_encode(["status"=>"success"]));
    }
}