<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 6/1/2018
 * Time: 11:58 PM
 */

class login extends CI_Controller
{
	public function __construct() {
        parent::__construct();
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Credentials: true ");
		header("Access-Control-Allow-Methods: OPTIONS, GET, POST");
		header("Access-Control-Allow-Headers: Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control");
	}
    public function index(){
        if($this->input->get('exit')==1){
            redirect('login/logout');
        }
        if($this->nativesession->userdata('userdata')){
            redirect('dashboard');
        }
        if($this->input->method()=="post"){
            $user_data = $this->m_user->login();
            if($user_data!=null){
                $this->nativesession->set_userdata('userdata', $user_data);
                $this->nativesession->set_userdata('logged_in', true);
                echo 1;
            }else{
                echo 0;
            }
            die();
        }
        $this->load->view('login');
    }
    public function logout(){
        $this->nativesession->destroy();
        redirect('login');
    }
    public function forgot(){
        $this->load->view('forgotpass');
    }
    public function sendMail(){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://api.mailjet.com/v3/send");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n    \"FromEmail\":\"sumantab.9@gmail.com\",\n    \"FromName\":\"Algosoft Support\",\n    \"Subject\":\"Hello World!\",\n    \"Text-part\":\"Dear passenger, welcome to Mailjet! May the delivery force be with you!\",\n    \"Html-part\":\"<h3>Dear passenger, welcome to Mailjet!</h3><br />May the delivery force be with you!\",\n    \"Recipients\":[\n        {\n            \"Email\": \"sumantab.9@live.in\"\n        }\n    ]\n  }");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, "a0d2eee83c79258b9fdc7cd523654c6b" . ":" . "26832b9e4c8961f1571618b5466346b7");

        $headers = array();
        $headers[] = "Content-Type: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        echo $result;
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);

        $result = $this->email->send();
    }
}