
<?php

class second_level_category extends CI_Controller
{
    public function all() {
        header('Content-type: application/json');
        echo json_encode($this->m_second_level_category->all());
    }

    public function get($id) {
        header('Content-type: application/json');
        echo json_encode($this->m_second_level_category->get($id));
    }

    public function sub_categories($id) {
        header('Content-type: application/json');
        echo json_encode($this->m_second_level_category->sub_categories($id));
    }
}