<?php

class category extends CI_Controller
{
    public function add(){
        if($this->input->method()=="post"){
            $post_data_arr = $this->input->post();
            
            if($post_data_arr["category"]["parent_id"] == "") {
                $post_data_arr["category"]["parent_id"] = null;
            }
            $this->m_category->create($post_data_arr["category"]);
            $category_id = $this->db->insert_id();

            $category_image_id = $this->do_upload("category_image");
            if($category_image_id != false) {
                $this->m_image->create(["image_id"=>$category_image_id,"category_id"=>$category_id]);
            }                

			$menu = $this->load->view('menu', null, true);
            $header = $this->load->view('header', null, true);
            $data = array(
                "menu"=>$menu,
                "header"=>$header
            );
            $this->load->view('category/category_add',$data);
        }
    }

    public function all(){
        $cats=$this->m_category->all();
		header('Content-Type: application/json');
        echo(json_encode($cats));
    }

    public function get($id){
		header('Content-Type: application/json');
        echo json_encode($this->m_category->get($id));
    }

    public function update($id, $selected_index){
		if($this->input->method()=="post") {
            $post_data_arr = $this->input->post();
            $this->m_category->update($id, $post_data_arr);
        }
		$menu = $this->load->view('menu', null, true);
        $header = $this->load->view('header', null, true);
        $data = array(
            "menu"=>$menu,
            "header"=>$header,
            "selected_index"=>$selected_index
        );
        $this->load->view('category/category_update',$data);
    }

	public function delete($id) {
        $this->m_category->delete($id);
        $menu = $this->load->view('menu', null, true);
        $header = $this->load->view('header', null, true);
        $data = array(
            "menu"=>$menu,
            "header"=>$header
        );
        $this->load->view('category/category_delete',$data);
    }
    public function get_number_of_items() {
        header('Content-type: Application/json');
        echo json_encode($this->m_category->get_number_of_items());
    }
    private function do_upload($name)
    {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['encrypt_name']        = true;
        /*$config['max_size']             = 100;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;*/

        $this->load->library('upload', $config);

        if ($this->upload->do_upload($name))
        {
            $data = $this->upload->data();
            return $data['file_name'];
        }
        else {
            return false;
        }
    }
}