<?php

class Dashboard extends CI_Controller
{
	public function index(){
		$menu = $this->load->view('menu', null, true);
		$header = $this->load->view('header', null, true);
		$data = array(
			"menu"=>$menu,
			"header"=>$header
		);
		$this->load->view('dashboard', $data);
  }
	
    public function dash(){
		$menu = $this->load->view('menu', null, true);
		$header = $this->load->view('header', null, true);
		$data = array(
			"menu"=>$menu,
			"header"=>$header
		);
		$this->load->view('dashboard', $data);
    }
	
		public function category_add(){
				$menu = $this->load->view('menu', null, true);
				$header = $this->load->view('header', null, true);
				$data = array(
					"menu"=>$menu,
					"header"=>$header
				);
				$this->load->view('category/category_add', $data);
		}

		public function category_update(){
			$menu = $this->load->view('menu', null, true);
			$header = $this->load->view('header', null, true);
			$data = array(
				"menu"=>$menu,
				"header"=>$header
			);
			$this->load->view('category/category_update', $data);
		}

		public function category_view(){
			$menu = $this->load->view('menu', null, true);
			$header = $this->load->view('header', null, true);
			$data = array(
				"menu"=>$menu,
				"header"=>$header
			);
			$this->load->view('category/category_view', $data);
		}
		
		public function category_delete(){
			$menu = $this->load->view('menu', null, true);
			$header = $this->load->view('header', null, true);
			$data = array(
				"menu"=>$menu,
				"header"=>$header
			);
			$this->load->view('category/category_delete', $data);
		}

		public function product_add(){
			$menu = $this->load->view('menu', null, true);
			$header = $this->load->view('header', null, true);
			$data = array(
				"menu"=>$menu,
				"header"=>$header
			);
			$this->load->view('product/product_add', $data);
			}
			
			public function product_view(){
				$menu = $this->load->view('menu', null, true);
				$header = $this->load->view('header', null, true);
				$data = array(
					"menu"=>$menu,
					"header"=>$header
				);
				$this->load->view('product/product_view', $data);
			}

			public function product_update(){
				$menu = $this->load->view('menu', null, true);
				$header = $this->load->view('header', null, true);
				$data = array(
					"menu"=>$menu,
					"header"=>$header
				);
				$this->load->view('product/product_update', $data);
			}

			public function product_delete(){
				$menu = $this->load->view('menu', null, true);
				$header = $this->load->view('header', null, true);
				$data = array(
					"menu"=>$menu,
					"header"=>$header
				);
				$this->load->view('product/product_delete', $data);
			}
			public function discount_add(){
				$menu = $this->load->view('menu', null, true);
				$header = $this->load->view('header', null, true);
				$data = array(
					"menu"=>$menu,
					"header"=>$header
				);
				$this->load->view('discount/discount_add', $data);
			}
			public function discount_update(){
				$menu = $this->load->view('menu', null, true);
				$header = $this->load->view('header', null, true);
				$data = array(
					"menu"=>$menu,
					"header"=>$header
				);
				$this->load->view('discount/discount_update', $data);
			}
			public function discount_delete(){
				$menu = $this->load->view('menu', null, true);
				$header = $this->load->view('header', null, true);
				$data = array(
					"menu"=>$menu,
					"header"=>$header
				);
				$this->load->view('discount/discount_delete', $data);
			}
			public function orders_view(){
				$menu = $this->load->view('menu', null, true);
				$header = $this->load->view('header', null, true);
				$data = array(
					"menu"=>$menu,
					"header"=>$header
				);
				$this->load->view('orders/orders_view', $data);
			}
			public function orders_update($order_id){
				$menu = $this->load->view('menu', null, true);
				$header = $this->load->view('header', null, true);
				$data = array(
					"menu"=>$menu,
					"header"=>$header,
					"order_id"=>$order_id
				);
				$this->load->view('orders/orders_update', $data);
			}
			public function banner_add(){
				$menu = $this->load->view('menu', null, true);
				$header = $this->load->view('header', null, true);
				$data = array(
					"menu"=>$menu,
					"header"=>$header
				);
				$this->load->view('banner/banner_add', $data);
			}
			public function banner_view(){
				$menu = $this->load->view('menu', null, true);
				$header = $this->load->view('header', null, true);
				$data = array(
					"menu"=>$menu,
					"header"=>$header
				);
				$this->load->view('banner/banner_view', $data);
			}
			public function banner_update($banner_id){
				$menu = $this->load->view('menu', null, true);
				$header = $this->load->view('header', null, true);
				$data = array(
					"menu"=>$menu,
					"header"=>$header,
					"banner_id"=>$banner_id
				);
				$this->load->view('banner/banner_update', $data);
			}
}