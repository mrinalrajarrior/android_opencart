<?php

class customer extends CI_Controller
{
    public function add(){
        if($this->input->method()=="post"){
            $post_data = file_get_contents('php://input');
            $post_data_arr = json_decode($post_data, true);
            $customer_token = $this->m_customer->search_customer($post_data_arr['customer']['token']);
            if($customer_token != Null) {
                header('Content-Type: application/json');
                echo json_encode($this->get($customer_token->customer_id));
            }
            else {
                $customer_arr = $post_data_arr["customer"];
                $this->m_customer->create($customer_arr);
                $customer_id = $this->db->insert_id();
                if(isset($post_data_arr["addresses"]) && sizeof($post_data_arr["addresses"])>0) {
                    foreach($post_data_arr["addresses"] as $address) {
                        $address_arr = $address;
                        $this->m_address->create($address_arr);
                        $address_id  = $this->db->insert_id();
                        $data = array(
                            "customer_id"=>$customer_id,
                            "address_id"=>$address_id
                        );
                        $this->m_customer_address_mapping->create($data);
                    }      
                }
                
                header('Content-Type: application/json');
                echo json_encode(["status"=>"inserted"]);
            }
            
        }
    }

    public function add_address(){
        if($this->input->method()=="post"){
            $post_data = file_get_contents('php://input');
            $post_data_arr = json_decode($post_data, true);
            $customer_id = $post_data_arr['customer_id'];
            unset($post_data_arr['customer_id']);
            $address_id = -1;
            if(isset($post_data_arr['address_id']) && $post_data_arr['address_id']!=null){
                $address_id = $post_data_arr['address_id'];
                unset($post_data_arr['address_id']);
                $this->db->where('address_id', $address_id);
                $this->db->update('address', $post_data_arr);
            }else{
                $this->db->insert('address', $post_data_arr);
                $address_id = $this->db->insert_id();
                $this->db->insert('customer_address_mapping', array(
                    "address_id" => $address_id,
                    "customer_id" => $customer_id
                ));
            }
            header('Content-Type: application/json');
            echo json_encode(["address_id" => $address_id]);
        }
    }

    public function get_addresses($customer_id){
        $this->db->where('customer_id', $customer_id);
        $this->db->join('address', 'customer_address_mapping.address_id = address.address_id');
        $addresses = $this->db->get('customer_address_mapping')->result();
        foreach ($addresses as $address) {
            $address->address_id = (string)$address->address_id;
        }
        header('Content-Type: application/json');
        echo json_encode(["addresses" => $addresses]);
    }

    public function all(){
        $this->load->helper('product_helper');
        $custs=$this->m_customer->all();
        $response = [];
        foreach($custs as $cust) {
            $addresses = $this->m_customer_address_mapping->get_addresses($cust->customer_id);
            $wishlists = $this->m_wishlist->get($cust->customer_id);
            
            foreach($wishlists as &$wishlist) {
                $wishlist["product"] = get_product($wishlist["product_id"]);
            }
            $res = array(
                "customer"=>$cust,
                "addresses"=>$addresses,
                "wishlist"=>$wishlists
            );
            array_push($response, $res);
        }
		header('Content-Type: application/json');
        echo(json_encode($response));
    }

    public function get($id){
        $this->load->helper('product_helper');
        $cust = $this->m_customer->get($id);
        $addresses = $this->m_customer_address_mapping->get_addresses($cust->customer_id);
        $wishlists = $this->m_wishlist->get($cust->customer_id);
        foreach($wishlists as &$wishlist) {
            $wishlist["product"] = get_product($wishlist["product_id"]);
        }
        $res = array(
            "customer"=>$cust,
            "addresses"=>$addresses,
            "wishlist"=>$wishlists
        );

		header('Content-Type: application/json');
        echo json_encode($res);
    }

    public function update($customer_id){
		if($this->input->method()=="post") {
            $post_data = file_get_contents('php://input');
            $post_data_arr = json_decode($post_data, true);
            if(isset($post_data_arr["customer"]) && sizeof($post_data_arr["customer"])>0) {
                $customer = $post_data_arr["customer"];
                $this->m_customer->update($customer_id, $customer);
            }
            if(isset($post_data_arr["address"]) && sizeof($post_data_arr["address"])>0) {
                $address = $post_data_arr["address"];
                if(isset($address["address_id"]) && sizeof($address["address_id"])>0) {
                    $this->m_address->update($address["address_id"],$address);
                }
                else {
                    $this->m_address->create($address);
                    $address_id  = $this->db->insert_id();
                    $data = array(
                        "customer_id"=>$customer_id,
                        "address_id"=>$address_id
                    );
                    $this->m_customer_address_mapping->create($data);
                }
            }
            
        }
		header('Content-Type: application/json');
        echo(json_encode(["affected_rows"=>$this->db->affected_rows()]));
    }

	public function delete($id) {
        $this->m_customer->delete($id);
        echo(json_encode(["status"=>"deleted"]));
    }
    
    public function login(){
		$data = json_decode(file_get_contents('php://input'), true);
		$customer_id = 0;
		$customer = [];

		if($data['type'] == "GOOGLE"){
			$customer = $this->db->get_where('customer', ['type'=>'GOOGLE', 'email'=>$data['email']])->row();
			if($customer==null){
				//Record not found, insert
				$this->db->insert('customer', $data);
				$customer_id = $this->db->insert_id();
			}else{
				$customer_id = $customer->customer_id;
				$this->db->where('customer_id', $customer_id);
				//Update new token, as token MIGHT change due to new phone
				$this->db->update("customer", ["token"=>$data['token']]);
			}
		}else if($data['type'] == "EMAIL"){
			$customer = $this->db->get_where('customer', ['type'=>'EMAIL', 'email'=>$data['email']])->row();
			if($customer!=null){
				$customer_id = $customer->customer_id;
				$requested_password = $data['password'];
				if(md5($requested_password) != $customer->password){
					$customer_id = -1;
				}
			}
		}
		header('Content-Type: application/json');
		if($customer_id>0){
			//If customer found
			$customer = $this->db->get_where('customer', ['customer_id'=>$customer_id])->row();
			$customer->password = null;
			echo json_encode([
				"status" => "SUCCESS",
				"customer" => $customer
			]);
		}else{
			echo json_encode([
				"status" => "FAILED",
				"message" => "Password mismatch"
			]);
		}
	}

	public function register(){
		$data = json_decode(file_get_contents('php://input'), true);
		header('Content-Type: application/json');
		$customer = $this->db->get_where('customer', ['type'=>'EMAIL', 'email'=>$data['email']])->row();
		if($customer != null){
			echo json_encode([
				"status" => "FAILED",
				"customer" => "Email already exists"
			]);
		}else{
			$data['password'] = md5($data['password']);
			$this->db->insert('customer', $data);
			$customer_id = $this->db->insert_id();
			$customer = $this->db->get_where('customer', ['customer_id'=>$customer_id])->row();
			$customer->password = null;
			echo json_encode([
				"status" => "SUCCESS",
				"customer" => $customer
			]);
		}
	}
}