<?php


class discount extends CI_Controller
{
    public function add(){
        if($this->input->method()=="post"){
            $post_data_arr = $this->input->post();
            $this->m_discount->create($post_data_arr);
            $insert_id =  $this->db->insert_id();
            $image_id = $this->do_upload();
            if($image_id != ""){
                $this->m_image->create(["image_id"=>$image_id,"discount_id"=>$insert_id]);
            }
            $menu = $this->load->view('menu', null, true);
            $header = $this->load->view('header', null, true);
            $data = array(
                "menu"=>$menu,
                "header"=>$header
            );
            $this->load->view('discount/discount_add',$data);
        }
    }

    public function all(){
        $prods=$this->m_discount->all();
        $response = (array)$prods;

		header('Content-Type: application/json');
        echo(json_encode($response));
    }

    public function get($id){
        $disc=(array)$this->m_discount->get($id);
		header('Content-Type: application/json');
        echo json_encode($disc);
    }

    public function update($id,$selected_index){
		if($this->input->method()=="post") {
            $post_data_arr = $this->input->post();
            $this->m_discount->update($id, $post_data_arr);
            
        }
		$menu = $this->load->view('menu', null, true);
        $header = $this->load->view('header', null, true);
        $data = array(
            "menu"=>$menu,
            "header"=>$header,
            "selected_index"=>$selected_index
        );
        $this->load->view('discount/discount_update',$data);
    }

	public function delete($id) {
        $this->m_discount->delete($id);
        $menu = $this->load->view('menu', null, true);
        $header = $this->load->view('header', null, true);
        $data = array(
            "menu"=>$menu,
            "header"=>$header,
        );
        $this->load->view('discount/discount_delete',$data);
    }
    
    private function do_upload()
    {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['encrypt_name']        = true;
        /*$config['max_size']             = 100;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;*/

        $this->load->library('upload', $config);

        if ($this->upload->do_upload("image"))
        {
            $data = $this->upload->data();
            return $data['file_name'];
        }
        else {
            return '';
        }
    }
}