<?php

class Tax extends CI_Controller
{
    public function add(){
        if($this->input->method()=="post"){
            $post_data_arr = $this->input->post();
           
            if(strlen($post_data_arr["banner_id"])==0){
                $banner_data = array(
                    "name"=>$post_data_arr["name"]
                );
                $this->m_banner->create($banner_data);
                $banner_id = $this->db->insert_id();

                $banner_discount_mapping_data = array(
                    "banner_id"=>$banner_id,
                    "discount_id"=>$post_data_arr["discount_id"]
                );
                $this->m_banner_discount_mapping->create($banner_discount_mapping_data);
                $image_id = $this->do_upload('image');
                if($image_id != "") {
                    $this->m_image->create(["image_id"=>$image_id,"banner_id"=>$banner_id,"url"=>"http://localhost/android/index.php/product/product_by_discount/".$post_data_arr["discount_id"]]);
                }
                //create
            }
            else{
                //update
                $banner_discount_mapping_data = array(
                    "banner_id"=>$post_data_arr["banner_id"],
                    "discount_id"=>$post_data_arr["discount_id"]
                );
                $this->m_banner_discount_mapping->create($banner_discount_mapping_data);
                $image_id = $this->do_upload('image');
                if($image_id != "") {
                    $this->m_image->create(["image_id"=>$image_id,"banner_id"=>$post_data_arr["banner_id"],"url"=>"http://localhost/android/index.php/product/product_by_discount/".$post_data_arr["discount_id"]]);
                }
            }

			$menu = $this->load->view('menu', null, true);
            $header = $this->load->view('header', null, true);
            $data = array(
                "menu"=>$menu,
                "header"=>$header
            );
            $this->load->view('banner/banner_add',$data);
        }
    }

    public function all(){
        $banners=$this->m_tax->all();
        
		header('Content-Type: application/json');
        echo(json_encode($banners));
    }

    public function get($id){
		header('Content-Type: application/json');
        echo json_encode($this->m_category->get($id));
    }

    public function update($id, $selected_index){
		if($this->input->method()=="post") {
            $post_data_arr = $this->input->post();
            $this->m_category->update($id, $post_data_arr);
        }
		$menu = $this->load->view('menu', null, true);
        $header = $this->load->view('header', null, true);
        $data = array(
            "menu"=>$menu,
            "header"=>$header,
            "selected_index"=>$selected_index
        );
        $this->load->view('category/category_update',$data);
    }

	public function delete($id) {
        $this->m_category->delete($id);
        $menu = $this->load->view('menu', null, true);
        $header = $this->load->view('header', null, true);
        $data = array(
            "menu"=>$menu,
            "header"=>$header
        );
        $this->load->view('category/category_delete',$data);
    }
    public function get_number_of_items() {
        header('Content-type: Application/json');
        echo json_encode($this->m_category->get_number_of_items());
    }
    private function do_upload($name)
    {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['encrypt_name']        = true;
        /*$config['max_size']             = 100;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;*/

        $this->load->library('upload', $config);

        if ($this->upload->do_upload($name))
        {
            $data = $this->upload->data();
            return $data['file_name'];
        }
        else {
            return false;
        }
    }
}