-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2019 at 10:15 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `opencart`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `address_id` int(11) NOT NULL,
  `address_line_1` varchar(255) NOT NULL,
  `address_line_2` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`address_id`, `address_line_1`, `address_line_2`, `zipcode`, `city`, `state`, `country`) VALUES
(1, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(2, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(3, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(4, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(5, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(6, 'kankarbagh', 'jawahar colony', '', 'patna', 'Bihar', 'India'),
(7, 'kankarbagh', 'jawahar colony', '', 'patna', 'Bihar', 'India'),
(8, 'kankarbagh', 'jawahar colony', '', 'patna', 'Bihar', 'India'),
(9, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(10, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(11, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Canada'),
(12, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Malaysia'),
(13, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(14, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Canada'),
(15, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Malaysia'),
(16, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(17, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Canada'),
(18, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Malaysia'),
(19, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(20, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Canada'),
(21, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Malaysia'),
(22, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(23, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Canada'),
(24, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Malaysia'),
(25, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(26, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Canada'),
(27, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Malaysia'),
(28, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(29, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Canada'),
(30, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Malaysia'),
(31, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(32, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Canada'),
(33, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Malaysia'),
(34, 'behala', 'jawahar colony', '', 'patna', 'Bihar', 'India'),
(35, 'Raurkela', 'jawahar colony', '', 'patna', 'Bihar', 'India');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `product_attribute_id` int(11) NOT NULL,
  `product_id` int(10) NOT NULL,
  `length` float DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `final_price` float DEFAULT NULL,
  `tax` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`product_attribute_id`, `product_id`, `length`, `size`, `color`, `quantity`, `price`, `final_price`, `tax`) VALUES
(2, 27, 42, 'L', 'Yellow', 25, 0, 0, 0),
(3, 28, 40, 'XL', 'White', 0, 0, 0, 0),
(4, 29, 42, 'XXL', 'Grey', 0, 0, 0, 0),
(5, 31, 40, 'L', 'Black', 0, 0, 0, 0),
(6, 32, 38, 'S', 'Red', 0, 0, 0, 0),
(7, 33, 40, 'L', 'Blue', 0, 0, 0, 0),
(8, 34, 42, 'XXL', 'White', 0, 0, 0, 0),
(9, 36, 44, 'XXL', 'Blue', 0, 0, 0, 0),
(10, 37, 40, 'L', 'Blue', 0, 0, 0, 0),
(11, 38, 40, 'L', 'green', 0, 0, 0, 0),
(12, 39, 38, 'S', 'Pink', 0, 0, 0, 0),
(13, 40, 32, 'S', 'Brown', 0, 0, 0, 0),
(14, 41, 40, 'L', 'Blue', 0, 0, 0, 0),
(15, 42, 44, 'XXL', 'Peach', 0, 0, 0, 0),
(16, 43, 34, 'S', 'Navy Blue', 0, 0, 0, 0),
(18, 46, 0, 'S', '', 0, 0, 0, 0),
(19, 47, 0, 'S', '', 0, 0, 0, 0),
(20, 48, 11, 'S', 'black', 50, 1200, 804, 0),
(21, 48, 20, 'S', 'white', 200, 60, 40.2, 0),
(22, 49, 0, 'S', '', 0, 0, 0, 0),
(23, 49, 0, 'S', '', 0, 0, 0, 0),
(24, 50, 0, 'S', '', 0, 0, 0, 0),
(25, 50, 0, 'S', '', 0, 0, 0, 0),
(26, 51, 0, 'S', '', 0, 0, 0, 0),
(27, 51, 0, 'S', '', 0, 0, 0, 0),
(28, 52, 0, 'S', '', 0, 0, 0, 0),
(29, 52, 0, 'S', '', 0, 0, 0, 0),
(30, 53, 0, 'S', '', 0, 0, 0, 0),
(31, 53, 0, 'S', '', 0, 0, 0, 0),
(32, 54, 0, 'S', '', 0, 0, 0, 0),
(33, 54, 0, 'S', '', 0, 0, 0, 0),
(34, 55, 0, 'S', '', 0, 0, 0, 0),
(35, 55, 0, 'S', '', 0, 0, 0, 0),
(36, 56, 0, 'S', '', 0, 0, 0, 0),
(37, 56, 0, 'S', '', 0, 0, 0, 0),
(38, 57, 0, 'S', '', 0, 0, 0, 0),
(39, 57, 0, 'S', '', 0, 0, 0, 0),
(40, 58, 5645, 'S', 'cvb c', 67566, 5464, 3, 0),
(41, 58, 546, 'S', 'bvn v', 7897, 456, 305.52, 0),
(52, 26, 500, 'XL', 'Orange', 124, 76, 45.6, 0),
(73, 26, 0, 'S', 'Blue', 23, 200, 120, 0),
(74, 27, 0, 'XXL', 'Red', 320, 250, 167.5, 0),
(75, 59, 45, 'S', 'BlueBlack', 22, 456, 305.52, 0),
(76, 60, 0, 'S', '', 0, 52, 34.84, 0),
(77, 61, 0, 'S', '', 0, 52, 34.84, 0),
(78, 62, 0, 'S', '', 0, 52, 34.84, 0),
(79, 63, 0, 'S', '', 0, 0, 0, 0),
(80, 64, 0, 'S', '', 0, 0, 0, 0),
(81, 65, 0, 'S', '', 0, 0, 0, 0),
(82, 66, 0, 'S', '', 0, 0, 0, 0),
(83, 67, 0, 'S', '', 0, 0, 0, 0),
(84, 68, 0, 'S', '', 0, 0, 0, 0),
(85, 69, 0, 'S', '', 0, 0, 0, 0),
(86, 70, 0, 'S', '', 0, 0, 0, 0),
(87, 71, 0, 'S', '', 0, 0, 0, 0),
(88, 72, 0, 'S', '', 0, 0, 0, 0),
(89, 73, 54, 'S', '', 5, 1000, 670, 0),
(90, 74, 52, 'S', 'red', 5, 200, 134, 0),
(91, 75, 56, 'S', '', 0, 300, 201, 0),
(92, 76, 0, 'S', '', 5, 120, 80.4, 0),
(93, 77, 0, 'S', '', 0, 123, 82.41, 0),
(94, 78, 0, 'S', '', 0, 1200, 948.72, 0.18),
(95, 79, 12, 'S', 'blue', 5, 1000, 790.6, 0.18),
(96, 79, 23, 'XL', 'Red', 50, 2000, 1, 0.18),
(97, 80, 0, 'S', '', 25, 2000, 1, 0.18);

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `banner_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`banner_id`, `name`) VALUES
(10, 'banner 1437'),
(11, 'new banner 1'),
(12, 'banner 0104'),
(13, 'banner 0106'),
(14, 'banner 0106'),
(15, 'banner 0107'),
(16, 'banner 01235');

-- --------------------------------------------------------

--
-- Table structure for table `banner_discount_mapping`
--

CREATE TABLE `banner_discount_mapping` (
  `banner_id` int(11) NOT NULL,
  `discount_id` int(11) NOT NULL,
  `image_id` varchar(500) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner_discount_mapping`
--

INSERT INTO `banner_discount_mapping` (`banner_id`, `discount_id`, `image_id`, `url`) VALUES
(8, 1, NULL, NULL),
(9, 1, NULL, NULL),
(10, 3, NULL, NULL),
(10, 6, NULL, NULL),
(10, 1, NULL, NULL),
(12, 1, NULL, NULL),
(12, 1, '322f458bb79f49be46e58344795b9697.png', 'http://localhost/android/index.php/product/product_by_discount/1'),
(13, 1, NULL, NULL),
(13, 1, '6f56dee8fab2cd4086c051fc36271897.png', 'http://localhost/android/index.php/product/product_by_discount/1'),
(14, 1, '505c19563c61d5c0cc11a6aee5014477.png', 'http://localhost/android/index.php/product/product_by_discount/1'),
(16, 3, '9dbf1e277d18499460506f8cda4b7755.png', 'http://localhost/android/index.php/product/product_by_discount/3'),
(16, 1, '2e5272e0e4a0aa1890de89edd62ab242.png', 'http://localhost/android/index.php/product/product_by_discount/1');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `customer_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`customer_id`, `cart_id`) VALUES
(10, 2),
(3, 5),
(21, 6);

-- --------------------------------------------------------

--
-- Table structure for table `cart_product_mapping`
--

CREATE TABLE `cart_product_mapping` (
  `cart_id` int(11) NOT NULL,
  `product_attribute_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_product_mapping`
--

INSERT INTO `cart_product_mapping` (`cart_id`, `product_attribute_id`, `quantity`) VALUES
(5, 5, 8),
(6, 16, 4);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `name`, `parent_id`) VALUES
(17, 'Men\'s T-shirts', 0),
(18, 'Casual Shirts', 0),
(19, 'Formal Shirt', 0),
(23, 'Dhoti', 0),
(25, 'Skirts', 0),
(26, 'Jeans & Leggings', 0),
(27, 'Sarrees', 0),
(28, 'Salwar Suits', 0),
(29, 'TopWear', 0),
(30, 'Burmudas', 0),
(31, 'Saree', 0),
(32, 'Saree', 0),
(33, 'Saree', 0),
(34, 'Pant', 0),
(35, 'Salwar', 0),
(36, 'category test 1', 0),
(37, '', 0),
(38, 'category test 2', 0),
(39, '', 0),
(40, '', 0),
(41, 'dsfsf', 0),
(42, 'Category 1', 0),
(43, 'Category 2', 0),
(44, 'Category 2', 0),
(45, 'Category 3', 0);

-- --------------------------------------------------------

--
-- Table structure for table `category_bkp2`
--

CREATE TABLE `category_bkp2` (
  `category_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_bkp2`
--

INSERT INTO `category_bkp2` (`category_id`, `name`, `parent_id`) VALUES
(1, 'Category 1', NULL),
(2, 'Category 1', NULL),
(3, 'Category 2', 0),
(4, 'Category 2', 0),
(5, 'Category 3', 0),
(6, 'Category 4', 3),
(7, 'Category 4', 3);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customer_id` int(10) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `token` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `firstname`, `lastname`, `phone`, `email`, `token`, `type`, `password`) VALUES
(2, 'Mrinal', 'Raj', 6290261655, 'mrinalraj@google.com', '', '', ''),
(3, 'Rahul', 'Rai', 6290261655, 'rajverma@google.com', '', '', ''),
(4, 'Rahul', 'Rai', 749137806, 'rajverma@google.com', '', '', ''),
(5, 'Rahul', 'Rai', 749137806, 'rajverma@google.com', '', '', ''),
(6, 'Rahul', 'Rai', 9279558348, 'rajverma@google.com', '', '', ''),
(7, 'Vishal', 'Verma', 6290261655, 'virajverma@google.com', '', '', ''),
(8, 'Vishal', 'Verma', 6290261655, 'virajverma@google.com', '', '', ''),
(9, 'Mrinal', 'Verma', 6290261655, 'virajverma@google.com', '', '', ''),
(10, 'Shailendra', 'Mishra', 6290261655, 'virajverma@google.com', '', '', ''),
(11, 'Ricky', 'Bahl', 6290261655, 'virajverma@google.com', 'AA1', '', ''),
(12, 'Ricky', 'Bahl', 6290261655, 'virajverma@google.com', 'AA1', '', ''),
(13, 'Ricky', 'Bahl', 6290261655, 'virajverma@google.com', 'AA1', '', ''),
(14, 'Ricky', 'Bahl', 6290261655, 'virajverma@google.com', 'AA1', '', ''),
(15, 'Ricky', 'Bahl', 6290261655, 'virajverma@google.com', 'AA1', '', ''),
(16, 'Ricky', 'Bahl', 6290261655, 'virajverma@google.com', 'AA1', '', ''),
(17, 'Pushpendra', 'Sharma', 6290261655, 'virajverma@google.com', 'AA2', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `customer_address_mapping`
--

CREATE TABLE `customer_address_mapping` (
  `customer_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_address_mapping`
--

INSERT INTO `customer_address_mapping` (`customer_id`, `address_id`) VALUES
(9, 9),
(10, 10),
(10, 11),
(10, 12),
(11, 13),
(11, 14),
(11, 15),
(12, 16),
(12, 17),
(12, 18),
(13, 19),
(13, 20),
(13, 21),
(14, 22),
(14, 23),
(14, 24),
(15, 25),
(15, 26),
(15, 27),
(16, 28),
(16, 29),
(16, 30),
(17, 31),
(17, 32),
(17, 33),
(5, 34),
(5, 35);

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE `discount` (
  `discount_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `discount_percentage` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discount`
--

INSERT INTO `discount` (`discount_id`, `name`, `discount_percentage`) VALUES
(1, 'Summer offer', 33),
(3, 'Bonaza Sale', 40),
(5, 'No Offer', 0),
(6, 'Maha Munch', 30),
(7, 'testing image', 11);

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `image_id` varchar(1000) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `banner_id` int(11) DEFAULT NULL,
  `url` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`image_id`, `category_id`, `discount_id`, `product_id`, `banner_id`, `url`) VALUES
('db228e7795cede83e8e493267d8e8e1c.png', NULL, NULL, 59, 0, NULL),
('e6d6eccf1250b0fd10dbf9f62758d991.png', NULL, NULL, 59, 0, NULL),
('41daba4ecf810ccc1e6628967c0d9965.jpg', NULL, NULL, 26, 0, NULL),
('dce16b9973d74a777e78135dd2e0e5ad.jpg', NULL, NULL, 26, 0, NULL),
('7d33b06dee54bc62d4816a34746ab4a0.jpg', NULL, NULL, 27, 0, NULL),
('ed757c7668edf6ecc7faf827656126dc.jpg', NULL, NULL, 27, 0, NULL),
('0120ca0abf2d090e64c04c213958bee0.png', NULL, NULL, NULL, 0, NULL),
('30658a6d28d14add16b14bebfbbd6d0a.png', NULL, NULL, NULL, 0, NULL),
('bbb8dd7230c5f4b5bdaf3f270d7ea8d8.png', 44, NULL, NULL, 0, NULL),
('e26454b2a8ec2464628edf0f683b1704.png', NULL, NULL, NULL, 0, NULL),
('ac135a8e27e8b30a3415e845a9bd2842.png', 45, NULL, NULL, 0, NULL),
('109d9afc8e131d20967e4dc17a01594e.png', 1, NULL, NULL, 0, NULL),
('4eae405954f7d506f04e6f5599cd54c6.png', 2, NULL, NULL, 0, NULL),
('888d994ab210149aedb6fbc1bc45050c.png', NULL, 7, NULL, 0, NULL),
('53dd7694953a13b271c093a08120fbb7.png', NULL, 8, NULL, 0, NULL),
('8c0561b4ea0da99fc53440c2806d8855.png', NULL, NULL, NULL, 8, NULL),
('b97aad6371c1bdd6579afff3a4d60b17.png', NULL, NULL, NULL, 9, NULL),
('76e036198c6104c5596d7bc8a25f6a74.png', NULL, NULL, NULL, 10, NULL),
('ce350e04c7d8f6e652d4acffdb6ed697.png', NULL, NULL, NULL, 10, NULL),
('98dfeb33c1a9271b20a1b74de098372d.png', NULL, NULL, NULL, 11, 'http://localhost/android/index.php/product/product_by_discount/1'),
('3665a036db9565482476179d75dece36.png', NULL, NULL, NULL, 11, 'http://localhost/android/index.php/product/product_by_discount/1'),
('66adb31fc8b28bed8f7551cdced9f8f9.png', NULL, NULL, NULL, 11, 'http://localhost/android/index.php/product/product_by_discount/6'),
('35d10462e8ddabd80695cb45ce16efd9.png', NULL, NULL, 73, NULL, NULL),
('ac6e58647f23c29659abbdde6aa577b4.png', NULL, NULL, 74, NULL, NULL),
('3490dbfc99a9487660fd8e1ce4df8d3c.png', NULL, NULL, 78, NULL, NULL),
('5f95b2cf7206aef1c199cbdd9c3b4609.png', NULL, NULL, 79, NULL, NULL),
('16d1e469a57a55c584d32f20b551a659.png', NULL, NULL, 79, NULL, NULL),
('1c503310cc2614bbaa25cda6d25ed6b0.png', NULL, NULL, 79, NULL, NULL),
('1b69220fee56baf9fae77e526db93edd.png', NULL, NULL, 80, NULL, NULL),
('322f458bb79f49be46e58344795b9697.png', NULL, NULL, NULL, 12, 'http://localhost/android/index.php/product/product_by_discount/1'),
('6f56dee8fab2cd4086c051fc36271897.png', NULL, NULL, NULL, 13, 'http://localhost/android/index.php/product/product_by_discount/1'),
('505c19563c61d5c0cc11a6aee5014477.png', NULL, NULL, NULL, 14, 'http://localhost/android/index.php/product/product_by_discount/1'),
('26e7df9779c0d191dd1db8e3682ce3c6.png', NULL, NULL, NULL, 15, 'http://localhost/android/index.php/product/product_by_discount/1'),
('7fcaacc9ec0e8304f8f89a9878b3fcda.png', NULL, NULL, NULL, 15, 'http://localhost/android/index.php/product/product_by_discount/6'),
('3262ddc8970eabbb136928ee46ed8fe9.png', NULL, NULL, NULL, 15, 'http://localhost/android/index.php/product/product_by_discount/3'),
('26e7df9779c0d191dd1db8e3682ce3c6.png', NULL, 1, NULL, 15, 'http://localhost/android/index.php/product/product_by_discount/1'),
('7baaab1d83ea2c75f0feb55e91576c4e.png', NULL, 6, NULL, 15, 'http://localhost/android/index.php/product/product_by_discount/6');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(20) NOT NULL,
  `estimated_time` int(5) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `status_id` int(5) DEFAULT '1',
  `date_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `address_line_1` varchar(255) DEFAULT NULL,
  `address_line_2` varchar(255) DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `shipping` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `estimated_time`, `amount`, `address_id`, `customer_id`, `status_id`, `date_added`, `date_modified`, `address_line_1`, `address_line_2`, `zipcode`, `city`, `state`, `country`, `shipping`) VALUES
(9, 2, 0, 0, 3, 1, '2019-04-17 18:25:18', '2019-04-17 18:25:18', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(10, 2, 0, 0, 19, 1, '2019-04-21 14:28:58', '2019-04-21 14:28:58', 'Raurkela', 'jawahar colony', NULL, 'patna', 'Bihar', 'India', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `status_id` int(5) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`status_id`, `status`) VALUES
(1, 'Placed'),
(3, 'Delivered'),
(4, 'Refund initiated'),
(2, 'Shipped'),
(5, 'Refund Completed');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image_id` varchar(255) NOT NULL,
  `price` int(10) NOT NULL,
  `tax` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `final_price` double NOT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `name`, `image_id`, `price`, `tax`, `company`, `final_price`, `tag`, `description`) VALUES
(26, 'Allen Solly Men\'s polo', 'da073ac90188d7b808f45608ea4a9fd7.jpg', 599, 'Tax Group 1', 'Allen Solly', 359.4, NULL, NULL),
(27, 'Peter England Men\'s Solid Regular fit Polo', '0435a096bb0fcefc2687cf8c9b912467.jpg', 799, 'Tax Group 1', 'Peter England', 535.3300170898438, NULL, NULL),
(28, 'AELO Men\'s Cotton T-Shirt', 'e3ccb119ddd7dbc042b03042abf4eef4.jpg', 474, 'Tax Group 1', 'AELO', 474, NULL, NULL),
(29, 'Pepe Jeans Men\'s Solid Regular fit Polo', '8b9da673df779d9f42c648d8a588d190.jpg', 1299, 'Tax Group 1', 'Pepe Jeans', 870.3300170898438, NULL, NULL),
(31, 'TOMMY HILFIGER Men\'s Solid Regular fit T-Shirt', 'c57448703497a9b9264b382715c96f38.jpg', 1999, 'Tax Group 1', 'TOMMY HILFIGER', 1199.4, NULL, NULL),
(32, 'Zaab Men\'s Casual Shirt', 'ad10c7a17fd49a6fc8a5e24998f62084.jpg', 649, 'Tax Group 1', 'Zaab', 649, NULL, NULL),
(33, 'Dennis Lingo Men\'s Cotton Casual Shirt', '5fe083bbc8b23d984f6dda5add1ee26d.jpg', 449, 'Tax Group 1', 'Dennis Lingo', 300.8299865722656, NULL, NULL),
(34, 'Campus Sutra Men\'s Checkered Casual Shirts', '0e3c8d4fbc82b4405856bba60981481a.jpg', 674, 'Tax Group 1', 'Campus Sutr', 451.5799865722656, NULL, NULL),
(36, 'Dennis Lingo Men\'s Cotton Casual Shirt', '9a9c2136156a96a3827bf35849e108e3.jpg', 1849, 'Tax Group 1', 'Dennis Lingo', 1109.4, NULL, NULL),
(37, 'Dennis Lingo Men\'s Cotton Casual Shirt', '66d54520b9c3d941e467701dd653d27c.jpg', 2499, 'Tax Group 1', 'Dennis Lingo', 1499.4, NULL, NULL),
(38, 'jwf Women\'s Long ndo western Traditional Skirt', '245d9845f71d1e4a710b193dae033f0a.jpg', 397, 'Tax Group 1', 'jwf', 397, NULL, NULL),
(39, 'Stars and You Formal Pencil skirt with Elastic Band', '0f1beebbab96536fb1ddc32c2fd655dc.jpg', 500, 'Tax Group 1', 'Stars', 500, NULL, NULL),
(40, 'N-Gal Women\'s High Waist Flared knit skater Short Mini Skirt-NAY110', 'c51ae58ff6ea0e2a24a9ad8151310b7c.jpg', 395, 'Tax Group 1', 'N-Gal', 395, NULL, NULL),
(41, 'Jaipuri Fashionista Cotton Regular Fit Jaipuri Printed Divider Palazzo Pant', '4c8cdd93c02e439dedf0773093fe3ebb.jpg', 355, 'Tax Group 1', 'Jipuri Fashionista', 355, NULL, NULL),
(42, 'FASHION CARE Women\'s Royal Crepe Skirt', 'cfd20fb6d1e51d25d4f1a03d6f8d5d91.jpg', 1998, 'Tax Group 1', 'Fashion Care', 1998, NULL, NULL),
(43, 'Navy Blue Small Dot skirt', 'e580c5fa317c6a060d2d9d0b7cf8ca82.jpg', 900, 'Tax Group 1', 'FabnFab', 396, NULL, NULL),
(45, 'Jeans', '', 0, '', 'Killer', 0, NULL, NULL),
(46, 'Jeans', '', 0, '', 'Killer', 0, NULL, NULL),
(47, 'Jeans', '', 0, '', 'Killer', 0, NULL, NULL),
(48, 'Socks', '', 0, '', 'Jockey', 0, NULL, NULL),
(49, '', '', 0, '', '', 0, NULL, NULL),
(50, '', '', 0, '', '', 0, NULL, NULL),
(51, '', '', 0, '', '', 0, NULL, NULL),
(52, '', '', 0, '', '', 0, NULL, NULL),
(53, '', '', 0, '', '', 0, NULL, NULL),
(54, '', '', 0, '', '', 0, NULL, NULL),
(55, '', '', 0, '', '', 0, NULL, NULL),
(56, '', '', 0, '', '', 0, NULL, NULL),
(57, '', '', 0, '', '', 0, NULL, NULL),
(58, 'Allen Solly Men\'s polo', '', 0, '', 'Stars', 0, NULL, NULL),
(59, 'Test product image', '', 0, '', 'comp1', 0, NULL, NULL),
(60, 'ckeditor', '', 0, '', '', 0, NULL, NULL),
(61, 'ckeditor', '', 0, '', '', 0, NULL, NULL),
(62, 'ckeditor', '', 0, '', '', 0, NULL, NULL),
(63, 'ckeditor test 2', '', 0, '', '', 0, NULL, '<h1><span class=\"marker\"><strong><em>This is testing throug ckeditor</em></strong></span></h1>\r\n'),
(64, 'ckeditor test 2', '', 0, '', '', 0, NULL, '<h1><span class=\"marker\"><strong><em>This is testing throug ckeditor</em></strong></span></h1>\r\n'),
(65, 'ckeditor test 2', '', 0, '', '', 0, NULL, '<h1><span class=\"marker\"><strong><em>This is testing throug ckeditor</em></strong></span></h1>\r\n'),
(66, 'ckeditor test 2', '', 0, '', '', 0, NULL, '<h1><span class=\"marker\"><strong><em>This is testing throug ckeditor</em></strong></span></h1>\r\n'),
(67, 'test 3 ckeditor', '', 0, '', '', 0, NULL, '<p><u>test 2</u></p>\r\n\r\n<p><u>test 2</u></p>\r\n'),
(68, 'test prod', '', 0, '', '', 0, NULL, ''),
(69, ' test prod 2', '', 0, '', '', 0, NULL, ''),
(70, ' test prod 2', '', 0, '', '', 0, NULL, ''),
(71, ' test prod 2', '', 0, '', '', 0, NULL, ''),
(72, ' test prod 2', '', 0, '', '', 0, NULL, ''),
(73, 'prod 1415', '', 0, '', '', 0, NULL, ''),
(74, 'prod 1447', '', 0, '', 'leo', 0, NULL, ''),
(75, 'prod 1449', '', 0, '', 'bb', 0, NULL, ''),
(76, 'prod 1458', '', 0, '', 'john miller', 0, NULL, ''),
(77, 'prod 1500', '', 0, '', 'blackberry', 0, NULL, ''),
(78, 'prod 1501', '', 0, '', 'blackberry', 0, NULL, ''),
(79, 'prod 1631', '', 0, '', 'Bata', 0, NULL, ''),
(80, 'prod 1635', '', 0, '', 'Peter England', 0, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `product_category_mapping`
--

CREATE TABLE `product_category_mapping` (
  `product_id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_category_mapping`
--

INSERT INTO `product_category_mapping` (`product_id`, `category_id`) VALUES
(26, 34),
(27, 23),
(28, 17),
(29, 17),
(31, 17),
(32, 18),
(33, 19),
(34, 18),
(36, 18),
(37, 18),
(38, 25),
(39, 25),
(40, 25),
(41, 25),
(42, 25),
(43, 25),
(46, 29),
(47, 29),
(48, 29),
(49, 29),
(50, 29),
(51, 29),
(52, 29),
(53, 29),
(54, 29),
(55, 29),
(56, 29),
(57, 29),
(58, 31),
(59, 30),
(60, 17),
(61, 17),
(62, 17),
(63, 17),
(64, 17),
(65, 17),
(66, 17),
(67, 17),
(69, 100),
(69, 400),
(70, 100),
(70, 400),
(71, 100),
(71, 400),
(72, 100),
(72, 400),
(74, 0),
(75, 0),
(76, 17),
(76, 18),
(76, 19),
(76, 23),
(77, 18),
(77, 19),
(77, 26),
(78, 17),
(78, 18),
(78, 19),
(78, 25),
(79, 17),
(79, 23),
(79, 26),
(80, 17),
(80, 19),
(80, 27);

-- --------------------------------------------------------

--
-- Table structure for table `product_discount_mapping`
--

CREATE TABLE `product_discount_mapping` (
  `product_id` int(10) NOT NULL,
  `discount_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_discount_mapping`
--

INSERT INTO `product_discount_mapping` (`product_id`, `discount_id`) VALUES
(26, 3),
(27, 1),
(28, 5),
(36, 3),
(37, 3),
(43, 4),
(31, 3),
(34, 1),
(29, 1),
(32, 5),
(33, 1),
(38, 0),
(39, 0),
(40, 0),
(42, 5),
(41, 5),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 6);

-- --------------------------------------------------------

--
-- Table structure for table `product_order_mapping`
--

CREATE TABLE `product_order_mapping` (
  `product_attribute_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_order_mapping`
--

INSERT INTO `product_order_mapping` (`product_attribute_id`, `order_id`, `quantity`) VALUES
(1, 8, 5),
(2, 8, 1),
(5, 9, 5),
(6, 9, 1),
(40, 10, 5),
(41, 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `second_level_category_bkp`
--

CREATE TABLE `second_level_category_bkp` (
  `category_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `second_level_category_bkp`
--

INSERT INTO `second_level_category_bkp` (`category_id`, `name`) VALUES
(0, 'Not Selected'),
(2, 'Men'),
(3, 'Women'),
(4, 'test1'),
(5, ''),
(6, 'test2'),
(7, ''),
(8, ''),
(9, 'fdfsd'),
(10, 'Parent 1'),
(11, 'Parent 2'),
(12, 'Parent 2'),
(13, 'Parent 3');

-- --------------------------------------------------------

--
-- Table structure for table `tax`
--

CREATE TABLE `tax` (
  `tax_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `gst` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tax`
--

INSERT INTO `tax` (`tax_id`, `name`, `cgst`, `sgst`, `gst`) VALUES
(1, 'Tax Group 2018-19', 0.09, 0.09, 0.18);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_attribute_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`customer_id`, `product_attribute_id`, `quantity`) VALUES
(3, 1, 3),
(3, 2, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`address_id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`product_attribute_id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `category_bkp2`
--
ALTER TABLE `category_bkp2`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`discount_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `second_level_category_bkp`
--
ALTER TABLE `second_level_category_bkp`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tax`
--
ALTER TABLE `tax`
  ADD PRIMARY KEY (`tax_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `product_attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `category_bkp2`
--
ALTER TABLE `category_bkp2`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `discount`
--
ALTER TABLE `discount`
  MODIFY `discount_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `second_level_category_bkp`
--
ALTER TABLE `second_level_category_bkp`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tax`
--
ALTER TABLE `tax`
  MODIFY `tax_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
