-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 24, 2019 at 10:35 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `opencart`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `address_id` int(11) NOT NULL,
  `address_line_1` varchar(255) NOT NULL,
  `address_line_2` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`address_id`, `address_line_1`, `address_line_2`, `zipcode`, `city`, `state`, `country`) VALUES
(1, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(2, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(3, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(4, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(5, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(6, 'kankarbagh', 'jawahar colony', '', 'patna', 'Bihar', 'India'),
(7, 'kankarbagh', 'jawahar colony', '', 'patna', 'Bihar', 'India'),
(8, 'kankarbagh', 'jawahar colony', '', 'patna', 'Bihar', 'India');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `product_id` int(10) NOT NULL,
  `length` float NOT NULL,
  `size` varchar(255) NOT NULL,
  `color` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`product_id`, `length`, `size`, `color`) VALUES
(26, 40, 'S', 'Black'),
(27, 42, 'L', 'Yellow'),
(28, 40, 'XL', 'White'),
(29, 42, 'XXL', 'Grey'),
(30, 40, 'L', 'Maroon'),
(31, 40, 'L', 'Black'),
(32, 38, 'S', 'Red'),
(33, 40, 'L', 'Blue'),
(34, 42, 'XXL', 'White'),
(36, 44, 'XXL', 'Blue'),
(37, 40, 'L', 'Blue'),
(38, 40, 'L', 'green'),
(39, 38, 'S', 'Pink'),
(40, 32, 'S', 'Brown'),
(41, 40, 'L', 'Blue'),
(42, 44, 'XXL', 'Peach'),
(43, 34, 'S', 'Navy Blue');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `name`) VALUES
(17, 'Men\'s T-shirts'),
(18, 'Casual Shirts'),
(19, 'Formal Shirt'),
(23, 'Dhoti'),
(25, 'Skirts'),
(26, 'Jeans & Leggings'),
(27, 'Sarrees'),
(28, 'Salwar Suits');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customer_id` int(10) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `firstname`, `lastname`, `phone`, `email`) VALUES
(2, 'Mrinal', 'Raj', 6290261655, 'mrinalraj@google.com'),
(3, 'Rahul', 'Rai', 6290261655, 'rajverma@google.com'),
(4, 'Rahul', 'Rai', 749137806, 'rajverma@google.com'),
(5, 'Rahul', 'Rai', 749137806, 'rajverma@google.com'),
(6, 'Rahul', 'Rai', 9279558348, 'rajverma@google.com'),
(7, 'Vishal', 'Verma', 6290261655, 'virajverma@google.com'),
(8, 'Vishal', 'Verma', 6290261655, 'virajverma@google.com');

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE `discount` (
  `discount_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `discount_percentage` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discount`
--

INSERT INTO `discount` (`discount_id`, `name`, `discount_percentage`) VALUES
(1, 'Summer offer', 33),
(3, 'Bonaza Sale', 40),
(5, 'No Offer', 0),
(6, 'Maha Munch', 30);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(20) NOT NULL,
  `estimated_time` int(5) NOT NULL,
  `amount` float NOT NULL,
  `address_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `status_id` int(5) NOT NULL DEFAULT '1',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `estimated_time`, `amount`, `address_id`, `customer_id`, `status_id`, `date_added`, `date_modified`) VALUES
(6, 2, 0, 0, 0, 2, '2019-03-24 15:37:25', '2019-03-24 21:33:52');

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `status_id` int(5) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`status_id`, `status`) VALUES
(1, 'Placed'),
(3, 'Delivered'),
(4, 'Refund initiated'),
(2, 'Shipped'),
(5, 'Refund Completed');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image_id` varchar(255) NOT NULL,
  `quantity` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `tax` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `final_price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `name`, `image_id`, `quantity`, `price`, `tax`, `company`, `final_price`) VALUES
(26, 'Allen Solly Men\'s polo', 'da073ac90188d7b808f45608ea4a9fd7.jpg', 20, 599, 'Tax Group 1', 'Allen Solly', 359.4),
(27, 'Peter England Men\'s Solid Regular fit Polo', '0435a096bb0fcefc2687cf8c9b912467.jpg', 12, 799, 'Tax Group 1', 'Peter England', 535.3300170898438),
(28, 'AELO Men\'s Cotton T-Shirt', 'e3ccb119ddd7dbc042b03042abf4eef4.jpg', 20, 474, 'Tax Group 1', 'AELO', 474),
(29, 'Pepe Jeans Men\'s Solid Regular fit Polo', '8b9da673df779d9f42c648d8a588d190.jpg', 15, 1299, 'Tax Group 1', 'Pepe Jeans', 870.3300170898438),
(30, 'Duke Men\'s Solid Slim fit T-Shirt', '9f7d1717da1bf70c8e0aef7464b28817.jpg', 22, 864, 'Tax Group 1', 'Duke', 864),
(31, 'TOMMY HILFIGER Men\'s Solid Regular fit T-Shirt', 'c57448703497a9b9264b382715c96f38.jpg', 10, 1999, 'Tax Group 1', 'TOMMY HILFIGER', 1199.4),
(32, 'Zaab Men\'s Casual Shirt', 'ad10c7a17fd49a6fc8a5e24998f62084.jpg', 12, 649, 'Tax Group 1', 'Zaab', 649),
(33, 'Dennis Lingo Men\'s Cotton Casual Shirt', '5fe083bbc8b23d984f6dda5add1ee26d.jpg', 15, 449, 'Tax Group 1', 'Dennis Lingo', 300.8299865722656),
(34, 'Campus Sutra Men\'s Checkered Casual Shirts', '0e3c8d4fbc82b4405856bba60981481a.jpg', 20, 674, 'Tax Group 1', 'Campus Sutr', 451.5799865722656),
(36, 'Dennis Lingo Men\'s Cotton Casual Shirt', '9a9c2136156a96a3827bf35849e108e3.jpg', 20, 1849, 'Tax Group 1', 'Dennis Lingo', 1109.4),
(37, 'Dennis Lingo Men\'s Cotton Casual Shirt', '66d54520b9c3d941e467701dd653d27c.jpg', 25, 2499, 'Tax Group 1', 'Dennis Lingo', 1499.4),
(38, 'jwf Women\'s Long ndo western Traditional Skirt', '245d9845f71d1e4a710b193dae033f0a.jpg', 40, 397, 'Tax Group 1', 'jwf', 397),
(39, 'Stars and You Formal Pencil skirt with Elastic Band', '0f1beebbab96536fb1ddc32c2fd655dc.jpg', 20, 500, 'Tax Group 1', 'Stars', 500),
(40, 'N-Gal Women\'s High Waist Flared knit skater Short Mini Skirt-NAY110', 'c51ae58ff6ea0e2a24a9ad8151310b7c.jpg', 20, 395, 'Tax Group 1', 'N-Gal', 395),
(41, 'Jaipuri Fashionista Cotton Regular Fit Jaipuri Printed Divider Palazzo Pant', '4c8cdd93c02e439dedf0773093fe3ebb.jpg', 15, 355, 'Tax Group 1', 'Jipuri Fashionista', 355),
(42, 'FASHION CARE Women\'s Royal Crepe Skirt', 'cfd20fb6d1e51d25d4f1a03d6f8d5d91.jpg', 10, 1998, 'Tax Group 1', 'Fashion Care', 1998),
(43, 'Navy Blue Small Dot skirt', 'e580c5fa317c6a060d2d9d0b7cf8ca82.jpg', 15, 900, 'Tax Group 1', 'FabnFab', 396);

-- --------------------------------------------------------

--
-- Table structure for table `product_category_mapping`
--

CREATE TABLE `product_category_mapping` (
  `product_id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_category_mapping`
--

INSERT INTO `product_category_mapping` (`product_id`, `category_id`) VALUES
(26, 18),
(27, 23),
(28, 17),
(29, 17),
(30, 17),
(31, 17),
(32, 18),
(33, 19),
(34, 18),
(36, 18),
(37, 18),
(38, 25),
(39, 25),
(40, 25),
(41, 25),
(42, 25),
(43, 25);

-- --------------------------------------------------------

--
-- Table structure for table `product_discount_mapping`
--

CREATE TABLE `product_discount_mapping` (
  `product_id` int(10) NOT NULL,
  `discount_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_discount_mapping`
--

INSERT INTO `product_discount_mapping` (`product_id`, `discount_id`) VALUES
(26, 3),
(27, 1),
(30, 5),
(28, 5),
(36, 3),
(37, 3),
(43, 4),
(31, 3),
(34, 1),
(29, 1),
(32, 5),
(33, 1),
(38, 0),
(39, 0),
(40, 0),
(42, 5),
(41, 5);

-- --------------------------------------------------------

--
-- Table structure for table `product_order_mapping`
--

CREATE TABLE `product_order_mapping` (
  `product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_order_mapping`
--

INSERT INTO `product_order_mapping` (`product_id`, `order_id`, `quantity`) VALUES
(30, 4, 0),
(31, 4, 0),
(32, 4, 0),
(30, 5, 0),
(31, 5, 0),
(32, 5, 0),
(26, 6, 5),
(27, 6, 1),
(28, 6, 7),
(29, 6, 2),
(26, 7, 5),
(27, 7, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`address_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`discount_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `discount`
--
ALTER TABLE `discount`
  MODIFY `discount_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
