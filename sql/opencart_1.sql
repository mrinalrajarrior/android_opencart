-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 16, 2019 at 07:29 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `opencart`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `customer_id` int(10) NOT NULL,
  `address_line_1` varchar(255) NOT NULL,
  `address_line_2` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`customer_id`, `address_line_1`, `address_line_2`, `zipcode`, `city`, `state`, `country`) VALUES
(8, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(0, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(0, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(0, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(2, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(3, 'kankarbagh', 'jawahar colony', '', 'patna', 'Bihar', 'India'),
(4, 'kankarbagh', 'jawahar colony', '', 'patna', 'Bihar', 'India'),
(5, 'kankarbagh', 'jawahar colony', '', 'patna', 'Bihar', 'India');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `product_id` int(10) NOT NULL,
  `length` float NOT NULL,
  `size` varchar(255) NOT NULL,
  `color` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`product_id`, `length`, `size`, `color`) VALUES
(6, 12, 'Not Sure', ''),
(10, 40, 'L', 'red'),
(11, 0, '0', 'red'),
(12, 12, 'XL', 'red'),
(13, 40, 'S', 'Black'),
(14, 40, 'S', 'Black'),
(15, 40, 'S', 'Black'),
(16, 40, 'S', 'Yellow'),
(17, 52, 'XL', 'blue'),
(18, 52, 'XL', 'blue'),
(19, 45, 'XL', 'Blue'),
(20, 0, 'S', ''),
(21, 0, 'S', ''),
(22, 32, 'S', 'Grey'),
(23, 40, 'L', 'blue');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `name`) VALUES
(1, 'Cat1'),
(3, 'Cat3'),
(4, 'Shirt'),
(5, 'Chinos'),
(6, 'Jeans'),
(7, 'Half Pant'),
(8, 'Full Pant');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customer_id` int(10) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `firstname`, `lastname`, `phone`, `email`) VALUES
(2, 'Mrinal', 'Raj', 6290261655, 'mrinalraj@google.com'),
(3, 'Rahul', 'Rai', 6290261655, 'rajverma@google.com'),
(4, 'Rahul', 'Rai', 749137806, 'rajverma@google.com'),
(5, 'Rahul', 'Rai', 749137806, 'rajverma@google.com'),
(6, 'Rahul', 'Rai', 9279558348, 'rajverma@google.com'),
(7, 'Vishal', 'Verma', 6290261655, 'virajverma@google.com'),
(8, 'Vishal', 'Verma', 6290261655, 'virajverma@google.com');

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE `discount` (
  `discount_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `discount_percentage` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discount`
--

INSERT INTO `discount` (`discount_id`, `name`, `discount_percentage`) VALUES
(1, 'Summer offer', 33),
(3, 'Bonaza Sale', 67);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(20) NOT NULL,
  `estimated_time` int(5) NOT NULL,
  `product_id` int(10) NOT NULL,
  `customer_id` int(10) NOT NULL,
  `amount` float NOT NULL,
  `quantity` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `estimated_time`, `product_id`, `customer_id`, `amount`, `quantity`) VALUES
(1, 2, 6, 2, 0, 0),
(2, 2, 6, 3, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image_id` varchar(255) NOT NULL,
  `quantity` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `tax` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `name`, `image_id`, `quantity`, `price`, `tax`, `company`) VALUES
(6, 'Tooth Brush', 'bb6e464005fea24b5786464ee9f3852b.jpg', 7, 30, 'Tax Group 2', 'colgate'),
(10, 'Chinese Collar', 'e4c84cd72679ed453833894f54a13339.jpg', 6, 2000, 'Tax Group 1', 'Benjamin'),
(11, 'Chinese Collar', '0', 6, 0, '0', ''),
(12, 'Sparrow Kurti', '98ab29ea33223602ad89c80afbacc1eb.png', 5, 32, 'Tax Group 1', 'john miller'),
(13, 'Tammered Jeans', 'edc2ae8bc0b5af0f9e43433191ef015e.jpg', 2, 2360, '', 'Turtle'),
(14, 'Tammered Jeans', '8b10157f55994a0a151da1f1183a2b92.jpg', 2, 2360, '? string: ?', 'Tortoise'),
(15, 'Tammered Jeans', 'a1bbb7f868687cd9a631c2ad5bb682a2.jpg', 2, 2360, '', 'Turtle'),
(16, 'Helicopter kurti', 'line 146', 12, 360, '', 'Arrow'),
(17, 'Redmi note 7', 'line 146', 5, 10000, '', 'Xiaomi'),
(18, 'Redmi note 8', 'line 146', 5, 10000, '', 'Xiaomi'),
(19, 'Football', 'line 146', 36, 2330, 'Tax Group 2', 'Manchester'),
(20, 'mongoose', 'line 146', 0, 0, 'Tax Group 1', ''),
(21, 'Hat', 'ad0570d00105151371be2be5a82057e0.jpg', 0, 0, 'Tax Group 1', ''),
(22, 'Shorts', 'e3dd115b123b08ac8ae445c90ebde85d.jpg', 7, 255, 'Tax Group 1', 'Blackberry'),
(23, 'Jeans New', '64a69d8430902a5b3522cf25a233ae2e.jpg', 9, 1002, 'Tax Group 2', 'Turtle');

-- --------------------------------------------------------

--
-- Table structure for table `product_category_mapping`
--

CREATE TABLE `product_category_mapping` (
  `product_id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_category_mapping`
--

INSERT INTO `product_category_mapping` (`product_id`, `category_id`) VALUES
(6, 3),
(10, 4),
(11, 4),
(12, 1),
(13, 2),
(14, 0),
(15, 2),
(16, 1),
(17, 8),
(18, 1),
(19, 5),
(20, 1),
(21, 1),
(22, 7),
(23, 6);

-- --------------------------------------------------------

--
-- Table structure for table `product_discount_mapping`
--

CREATE TABLE `product_discount_mapping` (
  `product_id` int(10) NOT NULL,
  `discount_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_discount_mapping`
--

INSERT INTO `product_discount_mapping` (`product_id`, `discount_id`) VALUES
(6, 3),
(10, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`discount_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `discount`
--
ALTER TABLE `discount`
  MODIFY `discount_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
