-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 21, 2019 at 06:38 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `opencart`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `address_id` int(11) NOT NULL,
  `address_line_1` varchar(255) NOT NULL,
  `address_line_2` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(50) NOT NULL,
  `customer_name` varchar(80) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `address_type` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`address_id`, `address_line_1`, `address_line_2`, `zipcode`, `city`, `state`, `country`, `customer_name`, `mobile`, `address_type`) VALUES
(1, 'Kolkata', 'Kaikhali Chiriamore', '700001', 'Kolkata', 'WB', 'India', 'Sumanta', '8697729362', 'Home'),
(2, 'Kolkata', 'Kaikhali Chiriamore', '700001', 'Kolkata', 'WB', 'IN', 'Sumanta', '8697729362', 'Home'),
(3, 'Kaikhali Chiriamore', 'Beside Meghna Apartment', '700136', 'Kolkata', 'West Bengal', 'India', 'Sumanta Banerjee', '7278075691', 'Home'),
(4, 'address line 1', 'address line 2', '790989', 'Mussoori', 'Uttrakhand', 'India', 'Testing ', '9876543210', 'Home');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `product_attribute_id` int(11) NOT NULL,
  `product_id` int(10) NOT NULL,
  `length` float DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `final_price` float DEFAULT NULL,
  `tax` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`product_attribute_id`, `product_id`, `length`, `size`, `color`, `quantity`, `price`, `final_price`, `tax`) VALUES
(1, 1, 42, 'S', 'White', 10, 800, 944, 0.18),
(2, 2, 42, 'S', 'White', 40, 799, 942.82, 0.18),
(3, 2, 44, 'M', 'White', 22, 799, 942.82, 0.18),
(4, 3, 10, 'M', 'White', 50, 1000, 700, 0.18),
(5, 3, 11, 'L', 'White', 22, 1200, 800, 0.18);

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `banner_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`banner_id`, `name`) VALUES
(1, 'Sample'),
(2, 'Sample-New');

-- --------------------------------------------------------

--
-- Table structure for table `banner_discount_mapping`
--

CREATE TABLE `banner_discount_mapping` (
  `banner_id` int(11) NOT NULL,
  `discount_id` int(11) NOT NULL,
  `image_id` varchar(500) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner_discount_mapping`
--

INSERT INTO `banner_discount_mapping` (`banner_id`, `discount_id`, `image_id`, `url`) VALUES
(1, 1, '512efaf52bdb35d788339a1a2725927b.jpg', 'http://localhost/android/index.php/product/product_by_discount/1'),
(2, 1, '8c13a354f8fd2709ad45586fd7080b2e.jpg', 'http://localhost/android/index.php/product/product_by_discount/1');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `customer_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`customer_id`, `cart_id`) VALUES
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `cart_product_mapping`
--

CREATE TABLE `cart_product_mapping` (
  `cart_id` int(11) NOT NULL,
  `product_attribute_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_product_mapping`
--

INSERT INTO `cart_product_mapping` (`cart_id`, `product_attribute_id`, `quantity`) VALUES
(2, 2, 1),
(2, 4, 1),
(2, 1, 1),
(2, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `name`, `parent_id`) VALUES
(1, 'T-Shirts', NULL),
(2, 'Shoes', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_bkp2`
--

CREATE TABLE `category_bkp2` (
  `category_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customer_id` int(10) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `token` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `firstname`, `lastname`, `phone`, `email`, `token`, `type`, `password`) VALUES
(1, 'sumanta banerjee', NULL, NULL, 'sumanta1234banerjee@gmail.com', '109506960277690729208', 'GOOGLE', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_address_mapping`
--

CREATE TABLE `customer_address_mapping` (
  `customer_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_address_mapping`
--

INSERT INTO `customer_address_mapping` (`customer_id`, `address_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE `discount` (
  `discount_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `discount_percentage` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discount`
--

INSERT INTO `discount` (`discount_id`, `name`, `discount_percentage`) VALUES
(1, 'No Offer', 0),
(3, 'Summer Sale', 35);

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `image_id` varchar(1000) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`image_id`, `category_id`, `discount_id`, `product_id`) VALUES
('05ab0852e5e2b7ece4217073efc8e74b.png', 1, NULL, NULL),
('b62b035cabf7f38b2c6f057ddcaa1c52.jpg', NULL, NULL, 1),
('1833852ae7f54fa62e39f3c80db8fea4.jpg', NULL, NULL, 2),
('563ae40bdbf36b906c0675fc0f9dce79.jpg', NULL, NULL, 2),
('f6f5b8312e96d4627a5fb154dbfce1e3.jpg', NULL, NULL, 2),
('8c82870c95f69c548fa7fd5360ac3a82.jpg', NULL, NULL, 2),
('70c24331c5d0764786ec468f213976e3.jpg', NULL, NULL, 2),
('4f70c2ee56bb1a45dbf672eefa9c206f.jpg', 2, NULL, NULL),
('df10312ef0acc8ed740c302512d2755e.jpg', NULL, NULL, 3),
('6a50ffc9a260001a3d3f22e22c60b4b1.jpg', NULL, NULL, 3),
('f57a41ae3945f7bd3b1595810ea9d41c.jpg', NULL, NULL, 3),
('1decc318d995eea1b386bc1580d1c73b.jpg', NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(20) NOT NULL,
  `estimated_time` int(5) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `status_id` int(5) DEFAULT '1',
  `date_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `address_line_1` varchar(255) DEFAULT NULL,
  `address_line_2` varchar(255) DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `shipping` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `estimated_time`, `amount`, `address_id`, `customer_id`, `status_id`, `date_added`, `date_modified`, `address_line_1`, `address_line_2`, `zipcode`, `city`, `state`, `country`, `shipping`) VALUES
(3, NULL, 3799, 1, 1, 1, '2019-09-21 04:23:04', '2019-09-21 04:23:04', 'Kolkata', 'Kaikhali Chiriamore', '700001', 'Kolkata', 'WB', 'India', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `status_id` int(5) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image_id` varchar(255) NOT NULL,
  `tax` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `name`, `image_id`, `tax`, `company`, `tag`, `description`) VALUES
(1, 'Product Name 1', '', '', 'Roadstar', NULL, '<h1><strong>Project Name</strong></h1>\r\n\r\n<p>Product Description</p>\r\n'),
(2, 'Product Name 2', '', '', 'Roadstar', NULL, '<h1>Roadster</h1>\r\n\r\n<h1>Men White &amp; Navy Blue Striped Polo T-shirt</h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4><strong>Complete The Look</strong></h4>\r\n\r\n<p>The feel of this Roadster tee is absolutely amazing thanks to the designer&#39;s use of cotton. This white s'),
(3, 'Shoe 1', '', '', 'Bata', NULL, '<p><strong>Product Name</strong><br />\r\nProduct Description....</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `product_category_mapping`
--

CREATE TABLE `product_category_mapping` (
  `product_id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_category_mapping`
--

INSERT INTO `product_category_mapping` (`product_id`, `category_id`) VALUES
(1, 1),
(2, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `product_discount_mapping`
--

CREATE TABLE `product_discount_mapping` (
  `product_id` int(10) NOT NULL,
  `discount_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_discount_mapping`
--

INSERT INTO `product_discount_mapping` (`product_id`, `discount_id`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_order_mapping`
--

CREATE TABLE `product_order_mapping` (
  `product_attribute_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_order_mapping`
--

INSERT INTO `product_order_mapping` (`product_attribute_id`, `order_id`, `quantity`) VALUES
(2, 1, 1),
(4, 1, 1),
(2, 2, 1),
(4, 2, 1),
(1, 2, 1),
(2, 3, 1),
(4, 3, 1),
(1, 3, 1),
(5, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `second_level_category_bkp`
--

CREATE TABLE `second_level_category_bkp` (
  `category_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tax`
--

CREATE TABLE `tax` (
  `tax_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `gst` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tax`
--

INSERT INTO `tax` (`tax_id`, `name`, `cgst`, `sgst`, `gst`) VALUES
(1, 'Tax Group 2018-19', 0.09, 0.09, 0.18);

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE `userinfo` (
  `UserName` varchar(50) DEFAULT NULL,
  `LoginName` varchar(50) DEFAULT NULL,
  `Password` varchar(50) DEFAULT NULL,
  `LoginType` varchar(50) DEFAULT NULL,
  `address` varchar(2500) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `doj` date DEFAULT NULL,
  `qualification` varchar(50) DEFAULT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `bankname` varchar(50) DEFAULT NULL,
  `accNo` varchar(50) DEFAULT NULL,
  `department` varchar(50) DEFAULT NULL,
  `panNo` varchar(50) DEFAULT NULL,
  `others` varchar(100) DEFAULT NULL,
  `dataLabel` varchar(30) DEFAULT NULL,
  `educative` varchar(30) DEFAULT NULL,
  `insurence` varchar(30) DEFAULT NULL,
  `otherG` varchar(30) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userinfo`
--

INSERT INTO `userinfo` (`UserName`, `LoginName`, `Password`, `LoginType`, `address`, `dob`, `mobile`, `doj`, `qualification`, `designation`, `bankname`, `accNo`, `department`, `panNo`, `others`, `dataLabel`, `educative`, `insurence`, `otherG`, `id`) VALUES
('admin', 'admin', 'admin', 'Admin', '----', '2018-06-24', '9876543210', '2018-06-24', NULL, 'admin', NULL, NULL, 'Department', NULL, 'admin@gmail.com', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_attribute_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`address_id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`product_attribute_id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `category_bkp2`
--
ALTER TABLE `category_bkp2`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`discount_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `second_level_category_bkp`
--
ALTER TABLE `second_level_category_bkp`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tax`
--
ALTER TABLE `tax`
  ADD PRIMARY KEY (`tax_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `product_attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `category_bkp2`
--
ALTER TABLE `category_bkp2`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `discount`
--
ALTER TABLE `discount`
  MODIFY `discount_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `second_level_category_bkp`
--
ALTER TABLE `second_level_category_bkp`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tax`
--
ALTER TABLE `tax`
  MODIFY `tax_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

INSERT INTO order_status (status_id, status) VALUES
(1, 'Placed'),
(3, 'Delivered'),
(4, 'Refund initiated'),
(2, 'Shipped'),
(5, 'Refund Completed');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
