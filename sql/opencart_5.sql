-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2019 at 08:37 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `opencart`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `address_id` int(11) NOT NULL,
  `address_line_1` varchar(255) NOT NULL,
  `address_line_2` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`address_id`, `address_line_1`, `address_line_2`, `zipcode`, `city`, `state`, `country`) VALUES
(1, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(2, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(3, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(4, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(5, '1st floor', 'sal lake', '', 'Kolkata', 'West Bengal', 'India'),
(6, 'kankarbagh', 'jawahar colony', '', 'patna', 'Bihar', 'India'),
(7, 'kankarbagh', 'jawahar colony', '', 'patna', 'Bihar', 'India'),
(8, 'kankarbagh', 'jawahar colony', '', 'patna', 'Bihar', 'India'),
(9, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(10, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(11, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Canada'),
(12, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Malaysia'),
(13, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(14, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Canada'),
(15, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Malaysia'),
(16, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(17, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Canada'),
(18, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Malaysia'),
(19, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(20, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Canada'),
(21, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Malaysia'),
(22, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(23, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Canada'),
(24, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Malaysia'),
(25, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(26, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Canada'),
(27, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Malaysia'),
(28, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(29, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Canada'),
(30, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Malaysia'),
(31, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'UK'),
(32, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Canada'),
(33, '1st floor', 'salt lake', '', 'Patna', 'West Bengal', 'Malaysia');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `product_attribute_id` int(11) NOT NULL,
  `product_id` int(10) NOT NULL,
  `length` float NOT NULL,
  `size` varchar(255) NOT NULL,
  `color` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`product_attribute_id`, `product_id`, `length`, `size`, `color`, `quantity`) VALUES
(1, 26, 40, 'S', 'Black', 0),
(2, 27, 42, 'L', 'Yellow', 0),
(3, 28, 40, 'XL', 'White', 0),
(4, 29, 42, 'XXL', 'Grey', 0),
(5, 31, 40, 'L', 'Black', 0),
(6, 32, 38, 'S', 'Red', 0),
(7, 33, 40, 'L', 'Blue', 0),
(8, 34, 42, 'XXL', 'White', 0),
(9, 36, 44, 'XXL', 'Blue', 0),
(10, 37, 40, 'L', 'Blue', 0),
(11, 38, 40, 'L', 'green', 0),
(12, 39, 38, 'S', 'Pink', 0),
(13, 40, 32, 'S', 'Brown', 0),
(14, 41, 40, 'L', 'Blue', 0),
(15, 42, 44, 'XXL', 'Peach', 0),
(16, 43, 34, 'S', 'Navy Blue', 0),
(17, 44, 0, 'S', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `customer_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`customer_id`, `cart_id`) VALUES
(10, 2),
(3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `cart_product_mapping`
--

CREATE TABLE `cart_product_mapping` (
  `cart_id` int(11) NOT NULL,
  `product_attribute_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_product_mapping`
--

INSERT INTO `cart_product_mapping` (`cart_id`, `product_attribute_id`, `quantity`) VALUES
(5, 5, 8);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `second_level_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `name`, `second_level_category_id`) VALUES
(17, 'Men\'s T-shirts', 0),
(18, 'Casual Shirts', 0),
(19, 'Formal Shirt', 0),
(23, 'Dhoti', 0),
(25, 'Skirts', 0),
(26, 'Jeans & Leggings', 0),
(27, 'Sarrees', 0),
(28, 'Salwar Suits', 0),
(29, 'TopWear', 1),
(30, 'Burmudas', 2),
(31, 'Saree', 2),
(32, 'Saree', 2),
(33, 'Saree', 2),
(34, 'Pant', 3),
(35, 'Salwar', 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customer_id` int(10) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `token` varchar(1000) NOT NULL,
  `type` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `firstname`, `lastname`, `phone`, `email`, `token`, `type`) VALUES
(2, 'Mrinal', 'Raj', 6290261655, 'mrinalraj@google.com', '', ''),
(3, 'Rahul', 'Rai', 6290261655, 'rajverma@google.com', '', ''),
(4, 'Rahul', 'Rai', 749137806, 'rajverma@google.com', '', ''),
(5, 'Rahul', 'Rai', 749137806, 'rajverma@google.com', '', ''),
(6, 'Rahul', 'Rai', 9279558348, 'rajverma@google.com', '', ''),
(7, 'Vishal', 'Verma', 6290261655, 'virajverma@google.com', '', ''),
(8, 'Vishal', 'Verma', 6290261655, 'virajverma@google.com', '', ''),
(9, 'Mrinal', 'Verma', 6290261655, 'virajverma@google.com', '', ''),
(10, 'Shailendra', 'Mishra', 6290261655, 'virajverma@google.com', '', ''),
(11, 'Ricky', 'Bahl', 6290261655, 'virajverma@google.com', 'AA1', ''),
(12, 'Ricky', 'Bahl', 6290261655, 'virajverma@google.com', 'AA1', ''),
(13, 'Ricky', 'Bahl', 6290261655, 'virajverma@google.com', 'AA1', ''),
(14, 'Ricky', 'Bahl', 6290261655, 'virajverma@google.com', 'AA1', ''),
(15, 'Ricky', 'Bahl', 6290261655, 'virajverma@google.com', 'AA1', ''),
(16, 'Ricky', 'Bahl', 6290261655, 'virajverma@google.com', 'AA1', ''),
(17, 'Pushpendra', 'Sharma', 6290261655, 'virajverma@google.com', 'AA2', '');

-- --------------------------------------------------------

--
-- Table structure for table `customer_address_mapping`
--

CREATE TABLE `customer_address_mapping` (
  `customer_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_address_mapping`
--

INSERT INTO `customer_address_mapping` (`customer_id`, `address_id`) VALUES
(9, 9),
(10, 10),
(10, 11),
(10, 12),
(11, 13),
(11, 14),
(11, 15),
(12, 16),
(12, 17),
(12, 18),
(13, 19),
(13, 20),
(13, 21),
(14, 22),
(14, 23),
(14, 24),
(15, 25),
(15, 26),
(15, 27),
(16, 28),
(16, 29),
(16, 30),
(17, 31),
(17, 32),
(17, 33);

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE `discount` (
  `discount_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `discount_percentage` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discount`
--

INSERT INTO `discount` (`discount_id`, `name`, `discount_percentage`) VALUES
(1, 'Summer offer', 33),
(3, 'Bonaza Sale', 40),
(5, 'No Offer', 0),
(6, 'Maha Munch', 30);

-- --------------------------------------------------------

--
-- Table structure for table `first_level_category`
--

CREATE TABLE `first_level_category` (
  `category_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(20) NOT NULL,
  `estimated_time` int(5) NOT NULL,
  `amount` float NOT NULL,
  `address_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `status_id` int(5) NOT NULL DEFAULT '1',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `estimated_time`, `amount`, `address_id`, `customer_id`, `status_id`, `date_added`, `date_modified`) VALUES
(6, 2, 0, 0, 10, 3, '2019-03-24 15:37:25', '2019-03-24 21:38:08');

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `status_id` int(5) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`status_id`, `status`) VALUES
(1, 'Placed'),
(3, 'Delivered'),
(4, 'Refund initiated'),
(2, 'Shipped'),
(5, 'Refund Completed');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image_id` varchar(255) NOT NULL,
  `price` int(10) NOT NULL,
  `tax` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `final_price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `name`, `image_id`, `price`, `tax`, `company`, `final_price`) VALUES
(26, 'Allen Solly Men\'s polo', 'da073ac90188d7b808f45608ea4a9fd7.jpg', 599, 'Tax Group 1', 'Allen Solly', 359.4),
(27, 'Peter England Men\'s Solid Regular fit Polo', '0435a096bb0fcefc2687cf8c9b912467.jpg', 799, 'Tax Group 1', 'Peter England', 535.3300170898438),
(28, 'AELO Men\'s Cotton T-Shirt', 'e3ccb119ddd7dbc042b03042abf4eef4.jpg', 474, 'Tax Group 1', 'AELO', 474),
(29, 'Pepe Jeans Men\'s Solid Regular fit Polo', '8b9da673df779d9f42c648d8a588d190.jpg', 1299, 'Tax Group 1', 'Pepe Jeans', 870.3300170898438),
(31, 'TOMMY HILFIGER Men\'s Solid Regular fit T-Shirt', 'c57448703497a9b9264b382715c96f38.jpg', 1999, 'Tax Group 1', 'TOMMY HILFIGER', 1199.4),
(32, 'Zaab Men\'s Casual Shirt', 'ad10c7a17fd49a6fc8a5e24998f62084.jpg', 649, 'Tax Group 1', 'Zaab', 649),
(33, 'Dennis Lingo Men\'s Cotton Casual Shirt', '5fe083bbc8b23d984f6dda5add1ee26d.jpg', 449, 'Tax Group 1', 'Dennis Lingo', 300.8299865722656),
(34, 'Campus Sutra Men\'s Checkered Casual Shirts', '0e3c8d4fbc82b4405856bba60981481a.jpg', 674, 'Tax Group 1', 'Campus Sutr', 451.5799865722656),
(36, 'Dennis Lingo Men\'s Cotton Casual Shirt', '9a9c2136156a96a3827bf35849e108e3.jpg', 1849, 'Tax Group 1', 'Dennis Lingo', 1109.4),
(37, 'Dennis Lingo Men\'s Cotton Casual Shirt', '66d54520b9c3d941e467701dd653d27c.jpg', 2499, 'Tax Group 1', 'Dennis Lingo', 1499.4),
(38, 'jwf Women\'s Long ndo western Traditional Skirt', '245d9845f71d1e4a710b193dae033f0a.jpg', 397, 'Tax Group 1', 'jwf', 397),
(39, 'Stars and You Formal Pencil skirt with Elastic Band', '0f1beebbab96536fb1ddc32c2fd655dc.jpg', 500, 'Tax Group 1', 'Stars', 500),
(40, 'N-Gal Women\'s High Waist Flared knit skater Short Mini Skirt-NAY110', 'c51ae58ff6ea0e2a24a9ad8151310b7c.jpg', 395, 'Tax Group 1', 'N-Gal', 395),
(41, 'Jaipuri Fashionista Cotton Regular Fit Jaipuri Printed Divider Palazzo Pant', '4c8cdd93c02e439dedf0773093fe3ebb.jpg', 355, 'Tax Group 1', 'Jipuri Fashionista', 355),
(42, 'FASHION CARE Women\'s Royal Crepe Skirt', 'cfd20fb6d1e51d25d4f1a03d6f8d5d91.jpg', 1998, 'Tax Group 1', 'Fashion Care', 1998),
(43, 'Navy Blue Small Dot skirt', 'e580c5fa317c6a060d2d9d0b7cf8ca82.jpg', 900, 'Tax Group 1', 'FabnFab', 396),
(44, 'Shoes', '', 0, 'Tax Group 1', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_category_mapping`
--

CREATE TABLE `product_category_mapping` (
  `product_id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_category_mapping`
--

INSERT INTO `product_category_mapping` (`product_id`, `category_id`) VALUES
(26, 18),
(27, 23),
(28, 17),
(29, 17),
(31, 17),
(32, 18),
(33, 19),
(34, 18),
(36, 18),
(37, 18),
(38, 25),
(39, 25),
(40, 25),
(41, 25),
(42, 25),
(43, 25),
(44, 34);

-- --------------------------------------------------------

--
-- Table structure for table `product_discount_mapping`
--

CREATE TABLE `product_discount_mapping` (
  `product_id` int(10) NOT NULL,
  `discount_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_discount_mapping`
--

INSERT INTO `product_discount_mapping` (`product_id`, `discount_id`) VALUES
(26, 3),
(27, 1),
(28, 5),
(36, 3),
(37, 3),
(43, 4),
(31, 3),
(34, 1),
(29, 1),
(32, 5),
(33, 1),
(38, 0),
(39, 0),
(40, 0),
(42, 5),
(41, 5),
(44, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_order_mapping`
--

CREATE TABLE `product_order_mapping` (
  `product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_order_mapping`
--

INSERT INTO `product_order_mapping` (`product_id`, `order_id`, `quantity`) VALUES
(30, 4, 0),
(31, 4, 0),
(32, 4, 0),
(30, 5, 0),
(31, 5, 0),
(32, 5, 0),
(26, 6, 5),
(27, 6, 1),
(28, 6, 7),
(29, 6, 2),
(26, 7, 5),
(27, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `second_level_category`
--

CREATE TABLE `second_level_category` (
  `category_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `second_level_category`
--

INSERT INTO `second_level_category` (`category_id`, `name`) VALUES
(1, 'Men'),
(2, 'Men'),
(3, 'Women');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_attribute_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`customer_id`, `product_attribute_id`, `quantity`) VALUES
(3, 1, 3),
(3, 2, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`address_id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`product_attribute_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`discount_id`);

--
-- Indexes for table `first_level_category`
--
ALTER TABLE `first_level_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `second_level_category`
--
ALTER TABLE `second_level_category`
  ADD PRIMARY KEY (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `product_attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `discount`
--
ALTER TABLE `discount`
  MODIFY `discount_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `first_level_category`
--
ALTER TABLE `first_level_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `second_level_category`
--
ALTER TABLE `second_level_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
