# DB Changes

## 22/09/2019 10:10 PM
``` SQL
ALTER TABLE orders ADD txn_id VARCHAR(255) NULL AFTER phone_number, ADD mode VARCHAR(50) NULL AFTER txn_id;
```

## 23/09/2019 10:00 AM
``` SQL
ALTER TABLE `orders` ADD `tax_amount` VARCHAR(20) NULL AFTER `mode`, ADD `total_amt` VARCHAR(20) NULL AFTER `tax_amount`, ADD `before_tax` VARCHAR(20) NULL AFTER `total_amt`, ADD `before_tax_with_shipping` VARCHAR(20) NULL AFTER `before_tax`;
```

## 27/09/2019 12:07 AM
```SQL
ALTER TABLE `orders` ADD `cancelled` INT NOT NULL DEFAULT '0' AFTER `before_tax_with_shipping`;
```